<?php

use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\Cliente;
class UsersDefault extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = [
            [
                'name' => 'root',
                'lastname'=>'raffa',
                'email' => 'ranaldoraffaele@gmail.com',
                'telefono' => '123456789',
                'password' =>  '39097456'
            ],
            [
                'name' => 'root',
                'lastname'=>'rosina',
                'email' => 'rosina@harasrapetti.com.uy',
                'telefono' => '123456789',
                'password' =>  'harasrapetti'
            ]
        ];

        User::insert($users);


    }
}

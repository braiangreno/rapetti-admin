<?php

use Illuminate\Database\Seeder;
use App\Models\Role;

class RolesTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = [
            [
                'name' => 'root',
                'name_show' => "Super Administrador"
            ],
            [
                'name' => 'administrator',
                'name_show' => "Administrador"
            ],
            [
                'name' => 'propietario',
                'name_show' => "Propietario"
            ]
        ];
        Role::insert($roles);
    }
}

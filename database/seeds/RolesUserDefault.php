<?php

use Illuminate\Database\Seeder;
use App\Models\RoleUser;
class RolesUserDefault extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = [
            [
                'user_id' => 1,
                'role_id' => 1
            ],
            [
                'user_id' => 2,
                'role_id' => 2
            ],

        ];

        RoleUser::insert($roles);
    }
}

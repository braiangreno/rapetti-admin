<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventEcografiaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('event_ecografia', function (Blueprint $table) {
            $table->increments('id');
            $table->date('fecha');
            $table->string('ovizq');
            $table->string('ovder');
            $table->string('utero');
            $table->string('observaciones');
            $table->string('foto_url');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('event_ecografia');
    }
}

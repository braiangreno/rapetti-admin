<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventVeterinariaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('event_veterinaria', function (Blueprint $table) {
            $table->increments('id');
            $table->date('fecha');
            $table->string('observaciones');
            $table->string('diagnostico');
            $table->string('fotos_id');
            $table->string('video_url');
            $table->string('tratamiento');
            $table->string('droga');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('event_veterinaria');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCaballosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('caballos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre');
            $table->integer('state');
            $table->integer('alimentacion');
            $table->integer('foto_perfil_id');
            $table->string('foto_perfil_url');
            $table->string('sexo');
            $table->string('pelo');
            $table->string('padre');
            $table->string('madre');
            $table->float('peso');
            $table->date('fecha_de_nacimiento');
            $table->date('fecha_de_ingreso');
            $table->string('observaciones');
            $table->string('observaciones_sanitarias');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('caballos');
    }
}

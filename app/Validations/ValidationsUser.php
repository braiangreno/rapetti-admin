<?php
/**
 * Created by PhpStorm.
 * User: raffa
 * Date: 21/9/2018
 * Time: 11:24 AM
 */



namespace App\Validations;

use Illuminate\Support\Facades\Validator;

/**
 * Description of ValidacionesUsuarios
 *
 * @author Gary Romero
 */
class ValidationsUser
{

    public static function store($data)
    {
        $rules = [
            //'username' => 'sometimes|max:255|unique:users,username',
            'name' => 'required|max:255',
            'telefono' => 'required|max:255',
            'lastname' => 'required|max:255',
            'email' => 'required|email|unique:users,email,NULL,id,deleted_at,NULL',
        ];
        $message=[
            'required'=>'El :atributte campo es requerido',
            'unique'=>'El Email ya existe',
        ];

        $validator = Validator::make($data, $rules,$message);
        return $validator;
    }

    public static function login($data)
    {
        $rules = [
            'email' => 'required|exists:users,email',
            'password' => 'required|min:4',
        ];

        $validator = Validator::make($data, $rules);

        return $validator;
    }

    public static function logout($data)
    {
        $rules = [
            'token' => 'required|min:60',
        ];

        $validator = Validator::make($data, $rules);
        return $validator;
    }

    public static function update($data)
    {
        $rules = [
            'id' => 'required|integer|exists:users,id',
            'name' => 'required|max:255',
            'email' => 'sometimes|email|max:255|unique:users,email',
            'password' => 'sometimes|min:4',
        ];

        $validator = Validator::make($data, $rules);
        return $validator;
    }

    public static function destroy($data)
    {
        $rules = [
            'id' => 'required|integer|exists:users,id,deleted_at,NULL',
        ];

        $validator = Validator::make($data, $rules);
        return $validator;
    }

}

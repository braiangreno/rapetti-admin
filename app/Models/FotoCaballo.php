<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FotoCaballo extends Model
{
    protected $table = 'fotos_asignadas';

    protected $primaryKey = "id";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'tipo', 'id_caballo','id_foto' 
    ];

    protected $dates = ['created_at', 'updated_at'];

    public $timestamps = true;

    public function fotos_asignadas(){
        return $this->belongsToMany('App\Models\FotoCaballo');
    }
}

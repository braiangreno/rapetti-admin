<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CaballoPropietario extends Model
{
    protected $table = 'caballos_propietarios';

    protected $primaryKey = "id";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id_propietario','id_caballo'
    ];

    protected $dates = ['created_at', 'updated_at'];

    public $timestamps = true;

    public function caballos_propietarios(){
        return $this->belongsToMany('App\Models\CaballoPropietario');
    }
}

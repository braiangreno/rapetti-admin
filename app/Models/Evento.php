<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Foto extends Model
{
    protected $table = 'fotos';

    protected $primaryKey = "id";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nombre', 'url', 
    ];

    protected $dates = ['created_at', 'updated_at'];

    public $timestamps = true;

    public function fotos(){
        return $this->belongsToMany('App\Models\Foto');
    }
}

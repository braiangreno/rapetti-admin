<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class VideoCaballo extends Model
{
    protected $table = 'videos_asignados';

    protected $primaryKey = "id";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'tipo', 'id_caballo','id_video' 
    ];

    protected $dates = ['created_at', 'updated_at'];

    public $timestamps = true;

    public function videos_asignados(){
        return $this->belongsToMany('App\Models\VideoCaballo');
    }
}

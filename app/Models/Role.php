<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $table = 'roles';

    protected $primaryKey = "id";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'name_show', 'description'
    ];

    protected $dates = ['created_at', 'updated_at'];

    public $timestamps = true;

    public function users(){
        return $this->belongsToMany('App\Models\User');
    }
}

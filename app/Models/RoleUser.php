<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class RoleUser extends Model
{
    use Notifiable, SoftDeletes;
    protected $table = 'rol_user';

    protected $primaryKey = "id";

    protected $fillable = [
        'user_id', 'role_id',
    ];

    protected $dates = ['created_at', 'updated_at'];


    public $timestamps = true;
    public function rolName()
    {

        return $this->hasOne('App\Models\Role', 'id', 'role_id');
    }


}

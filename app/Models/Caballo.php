<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Caballo extends Model
{
    protected $table = 'caballos';

    protected $primaryKey = "id";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nombre', 'foto_perfil_id','foto_perfil_url','sexo','pelo','padre','madre','peso','state','alimentacion','fecha_de_nacimiento','fecha_de_ingreso','observaciones','observaciones_sanitarias'
    ];

    protected $dates = ['created_at', 'updated_at'];

    public $timestamps = true;

    public function caballos(){
        return $this->belongsToMany('App\Models\Caballo');
    }
}

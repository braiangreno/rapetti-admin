<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class Authenticate
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = Auth::user();

        if (isset($user) && (!is_null($user) || !empty($user)))
                   return $next($request);

       // Session::flash('error', trans('generals.not_login'));

        return redirect()->route('web');
    }
}

<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\RoleUser;
use App\Models\Caballo;
use App\Models\Foto;
use App\Models\FotoCaballo;
use App\Models\Video;
use App\Models\VideoCaballo;
use DB;
use App\Models\CaballoPropietario;
use App\Validations\ValidationsUser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Redirect;
use Validator;
use Session;
 
class CaballosController extends Controller
{
     public function index(Request $request)
    {       
        if(isset($request->search_prop)){

            if($request->search_prop === ''){
                $caballos = Caballo::orderBy('created_at', 'desc')->get();
                $list=RoleUser::where('role_id','3')->select('user_id as id')->get()->toArray();
                $usuarios = User::whereIn('id',$list)->orderBy('name')->get();
                     
                return view('caballos.index',['caballos'=>$caballos,'propietarios'=>$usuarios]);

            }
            
            $caballos = DB::table('caballos_propietarios')
            ->join('caballos','caballos_propietarios.id_caballo', '=', 'caballos.id')
            ->join('users', 'caballos_propietarios.id_propietario', '=', 'users.id')
            ->select('caballos.*')
            ->where('users.id',$request->search_prop)
            ->orderBy('created_at', 'desc')
            ->get();
            $list=RoleUser::where('role_id','3')->select('user_id as id')->get()->toArray();
            $usuarios = User::whereIn('id',$list)->orderBy('name')->get();
            
            return view('caballos.index',['caballos'=>$caballos,
            'propietarios'=>$usuarios,'user_id'=>$request->search_prop]);

        }else{
            $caballos = Caballo::orderBy('created_at', 'desc')->get();
            $list=RoleUser::where('role_id','3')->select('user_id as id')->get()->toArray();
            $usuarios = User::whereIn('id',$list)->orderBy('name')->get();
                 
            return view('caballos.index',['caballos'=>$caballos,'propietarios'=>$usuarios]);

        }
 

    }
    public function propietario_caballo($id){
     
        $propietarios = DB::table('caballos_propietarios')->where('id_caballo',$id)->join('users', 'users.id', '=', 'caballos_propietarios.id_propietario')->get();
        $array_prop=array();
        $list=RoleUser::where('role_id','3')->select('user_id as id')->get()->toArray();
        $list_pID=array();
        foreach($propietarios as $p){
            $array_prop[]=strval($p->id_propietario).",".$p->name." ".$p->lastname;
            $list_pID[]=$p->id_propietario;
        }
        $val1=implode("|", $array_prop);
        
        $usuarios = User::whereIn('id',$list)->whereNotIn('id',$list_pID)->get();

        $array_usuarios=array();
        foreach($usuarios as $p){
            $array_usuarios[]=strval($p->id).",".$p->name." ".$p->lastname;
        }

        $val2=implode("|", $array_usuarios);
        
        return $val1."+".$val2;
    }
    public function propietario()
    {        
        $user = Auth::user();
        $mis_caballos_id=CaballoPropietario::where('id_propietario',$user->id)->select('id_caballo as id')->get()->toArray();

        $caballos = Caballo::whereIn('id',$mis_caballos_id)->get();
        return view('propietario.index',['caballos'=>$caballos]);
    }
    public function perfil($id) 
    {        
        $pesos_min_t = DB::table('tabla_pesos_general')->where('tipo',1)->get();
        $peso_min=[];
        foreach($pesos_min_t as $p){$peso_min[$p->mes]=$p->peso;}
        $pesos_max_t = DB::table('tabla_pesos_general')->where('tipo',2)->get();
        $peso_max=[];
        foreach($pesos_max_t as $p){$peso_max[$p->mes]=$p->peso;}
        $caballo = Caballo::where('id',$id)->first();
        $eventos_caballo=DB::table('eventos')->where('id_caballo',$id)->get();
        $eventos_eco=count(DB::table('eventos')->where('id_caballo',$id)->where('tipo','3')->get());
       
        $campos=DB::table('peso_potrillo')->where('caballo_id',$id)->orderby('mes')->get();

        if(count($campos)==0){
            $pesos=array();
            }else{
                $pesos=$campos;
            }
    
        return view('caballos.perfil',['evento_eco'=>$eventos_eco,'caballo'=>$caballo,'id'=>$id,'eventos'=>$eventos_caballo,'pesos'=> $pesos ,'peso_max'=>$peso_max,'peso_min'=>$peso_min]);
    }

    public function perfil_prop($id)
    {        
        $pesos_min_t = DB::table('tabla_pesos_general')->where('tipo',1)->get();
        $peso_min=[];
        foreach($pesos_min_t as $p){$peso_min[$p->mes]=$p->peso;}
        $pesos_max_t = DB::table('tabla_pesos_general')->where('tipo',2)->get();
        $peso_max=[];
        foreach($pesos_max_t as $p){$peso_max[$p->mes]=$p->peso;}
        $caballo = Caballo::where('id',$id)->first();
        $eventos_caballo=DB::table('eventos')->where('id_caballo',$id)->get();
        $eventos_eco=count(DB::table('eventos')->where('id_caballo',$id)->where('tipo',3)->get());
       
        $eventos_foto=DB::table('eventos')->where('id_caballo',$id)->where('tipo',6)->select("tipo_event_id as id")->get()->toArray();
        $ids=[];

        foreach($eventos_foto as $f){
                $ids[]=$f->id;
        }
        $fotos_data=DB::table('event_foto')->whereIn('id',$ids)->get();


        $asigne=FotoCaballo::where('id_caballo',$id)->select("id_foto as id")->get()->toArray();
        $fotos=Foto::whereIn('id',$asigne)->get();
        $campos=DB::table('peso_potrillo')->where('caballo_id',$id)->orderby('mes')->get();

        if(count($campos)==0){
            $pesos=array();
            }else{
                $pesos=$campos;
            }
         
        return view('propietario.perfil',['caballo'=>$caballo,'id'=>$id,'evento_eco'=>$eventos_eco,'eventos'=>$eventos_caballo,'fotos_data'=>$fotos_data,'fotos'=>$fotos,'pesos'=> $pesos ,'peso_max'=>$peso_max,'peso_min'=>$peso_min]);
    }
    public function register(Request $request) {
        
    
     $data=[
        'nombre'=>'',
        'foto_perfil_id'=>0,
        'state'=>1,
        'alimentacion'=>1,
        'foto_perfil_url'=>'',
        'sexo'=>'',
        'pelo'=>'',
        'padre'=>'',
        'madre'=>'',
        'peso'=>'',
        'fecha_de_nacimiento'=>'',
        'fecha_de_ingreso'=>'',
        'observaciones'=>'',
        'observaciones_sanitarias'=>''
     ];
            $data_in=$request->all();
                
            if(!empty($request->input('Observaciones2'))) {
                $data['observaciones']=$data_in['Observaciones2'];
            }

            if(!empty($request->input('Observaciones2'))) {
                $data['observaciones_sanitarias']=$data_in['Observaciones2'];
            }

            $image = $request->file('e2_file');
            $image_name=rand();
            $new_name = $image_name . '.' . $image->getClientOriginalExtension();
            $image->move(public_path('imagenes_subidas'), $new_name);
            $data['nombre']=$data_in['Nombre'];
            $data['sexo']=$data_in['Sexo'];
            $data['pelo']=$data_in['Pelo'];
            $data['padre']=$data_in['Padre'];
            $data['madre']=$data_in['Madre'];
            $data['peso']=$data_in['Peso'];
            $data['state']=1;
            $data['alimentacion']=$data_in['alimentacion'];
            $data['fecha_de_nacimiento']= $data_in['fechanacimiento'];
            $data['fecha_de_ingreso']=$data_in['fechaingreso'];
            $data['foto_perfil_url']="/imagenes_subidas"."/".$new_name;
            $fecha_hoy=date("Y-m-d");
             $imagen= Foto::create([
                'nombre'=>$new_name,
                'url' =>"/imagenes_subidas"."/".$new_name
            ]);
            $data['foto_perfil_id']=$imagen->id;
             $caballo=Caballo::create($data);
         
            FotoCaballo::create([
                'tipo' => '1',
                'id_caballo'=>$caballo->id,
                'id_foto'=>$imagen->id
            ]);
             foreach($data_in['propietarios'] as &$val)
                {
                    CaballoPropietario::create([
                            'id_propietario' => $val,
                            'id_caballo'=>$caballo->id,
                        ]);
                }       

                $id_evento=DB::table('event_peso')->insertGetId(
                    array('fecha'=>$fecha_hoy, 'peso'=> $data['peso'])
                );
                DB::table('eventos')->insertGetId(
                    array(
                        'id_caballo'=>$caballo->id,
                        'fecha'=>$fecha_hoy,
                        'tipo'=>1,
                        'tipo_event_id'=>$id_evento,
                     )
                );

                Session::flash('message', '¡Registro completado!');
                return redirect('/caballos'); 
              
    }
    public function create() {

        $list=RoleUser::where('role_id','3')->select('user_id as id')->get()->toArray();
        $usuarios = User::whereIn('id',$list)->orderBy('name')->get();
       
        return view('caballos.crear',['propietarios'=>$usuarios]);
    }
    public function update(Request $request) {
        $data_in=$request->all();
        $id=$data_in['id'];
        $propietarios= $data_in['propietarios'];
        unset($data_in['id']);
        unset($data_in['propietarios']);
     

        if(empty($request->input('observaciones'))) {
            $data_in['observaciones']="";
        }

        DB::table('caballos_propietarios')->where('id_caballo', '=', $id)->delete();

        foreach($propietarios as &$val)
        {
            CaballoPropietario::create([
                    'id_propietario' => $val,
                    'id_caballo'=>$id,
                ]);
        }     
        Caballo::whereId($id)->update($data_in);  
      
    }

    public function imagenes(Request $request,$id){
        $asigne=FotoCaballo::where('id_caballo',$id)->select("id_foto as id")->get()->toArray();;
        $fotos=Foto::whereIn('id',$asigne)->get();
     
        return view('caballos.galeriadeimagenes',['fotos'=>$fotos,'id'=>$id]);
    }
    public function videos(Request $request,$id){
        $asigne=VideoCaballo::where('id_caballo',$id)->select("id_video as id")->get()->toArray();;
        $videos=Video::whereIn('id',$asigne)->get();
        
        return view('caballos.galeriadevideos',['videos'=>$videos,'id'=>$id,'permiso'=>0]);
    }
    public function videos2(Request $request,$id){
        $asigne=VideoCaballo::where('id_caballo',$id)->select("id_video as id")->get()->toArray();;
        $videos=Video::whereIn('id',$asigne)->get();
        
        return view('caballos.galeriadevideos',['videos'=>$videos,'id'=>$id,'permiso'=>1]);
    }

    public function destroy(Request $request, $id) {
        
        Caballo::whereId($id)->delete();
        CaballoPropietario::where('id_caballo',$id)->delete();
        return back();
     
    }
    public function activar(Request $request) {
        $data_in=$request->all();
        if($data_in['type']){
            Caballo::whereId($data_in['id'])->where('state','1')->update(['state' => '0']);  
        }else{
            Caballo::whereId($data_in['id'])->where('state','0')->update(['state' => '1']); 
        }
        
   
        return response()->json(['state' => 'success', 'data' => 'ok'], 200);

    }
    public function alimentacion(Request $Request){
        $caballos = Caballo::where('state','1')->get();
        return view('caballos.alimentacion',['caballos'=>$caballos]);
    }

    public function alimentacionchange(Request $request){
        $data_in=$request->all();
      
        foreach($data_in['caballos'] as &$val)
        {
            Caballo::whereId($val)->update(['alimentacion' => $data_in['alimentacion']]);  
        }     
               

        return redirect()->route('caballos.index');     
    }


    public function pesos_potrillo(Request $request){
    
        /*

        DB::table('peso_potrillo')->insertGetId(
            array(
                'id_caballo'=>$data_in['caballo'],
                'fecha'=>$data_in['fecha'],
                'tipo'=>2,
                'tipo_event_id'=>$id_evento,
            )
        ); */
        $caballos = Caballo::where('state','1')->get();

        return view('caballos.peso_index',['caballos'=>$caballos]);
    }

    
    public function pesos_tabla(Request $request){
    
  
        $pesos_min = DB::table('tabla_pesos_general')->where('tipo',1)->get();
        $pesos_max = DB::table('tabla_pesos_general')->where('tipo',2)->get();
     
        return view('caballos.peso_general',['pesos_min'=>$pesos_min ,'pesos_max'=>$pesos_max ]);
    }
    public function pesos_tabla_update(Request $request){
    
        $data_in=$request->all();
        for ($i = 1; $i <= 22; $i++) {
            DB::table('tabla_pesos_general')->where('mes',$i)->where('tipo',1)->update(
                array('peso'=> $data_in['mes_'.$i.'_1'])
            );
            DB::table('tabla_pesos_general')->where('mes',$i)->where('tipo',2)->update(
                array('peso'=> $data_in['mes_'.$i.'_2'])
            );
        }

        $pesos_min = DB::table('tabla_pesos_general')->where('tipo',1)->get();
        $pesos_max = DB::table('tabla_pesos_general')->where('tipo',2)->get();
     
        return view('caballos.peso_general',['pesos_min'=>$pesos_min ,'pesos_max'=>$pesos_max ]);
    }
    
    public function pesos_potrillo_get(Request $request, $id){
        


        $campos=DB::table('peso_potrillo')->where('caballo_id',$id)->orderby('mes')->get();
        if(count($campos)==0){
    
            $pesos=array();
            for ($i = 1; ; $i++) {
                if ($i > 22) {
                    break;
                }
                $object = (object) [
                    'peso' => 0,
                    'mes' => $i,
                  ];

                $pesos[$i]= $object;
            }
            return view('caballos.peso_table',['pesos'=>$pesos,'state'=>1]);
        }else{
            return view('caballos.peso_table',['pesos'=>$campos,'state'=>0]);
        }

    }
    public function pesos_potrillo_update(Request $request){
           
        $data_in=$request->all();
        if($data_in==[])return;
        for ($i = 1; ; $i++) {
            if ($i > 22) {
                break;
            }
            if($data_in['state']==1){
                DB::table('peso_potrillo')->insert(
                    array('caballo_id'=>$data_in['caballo'],'mes'=>$i, 'peso'=> $data_in['mes_'.$i])
                );
            }else{
                DB::table('peso_potrillo')
                            ->where('caballo_id',$data_in['caballo'])   
                            ->where('mes',$i)
                            ->update(
                                array('peso'=> $data_in['mes_'.$i])
                            );
            }
           
            
        }   

        return Redirect::away('pesos');
    }
    public function pedigree_update(Request $request , $id){


           
      
                $image = $request->file('file_pedigree');
                $image_name=rand();
                $new_name = $image_name . '.' . $image->getClientOriginalExtension();
                $image->move(public_path('pdf_subidos'), $new_name);
                $url="/pdf_subidos"."/".$new_name ;
              
       
                Caballo::whereId($id)->update(array('pedigree' => "/pdf_subidos"."/".$new_name));  
                return redirect()->back();
           

    }
    public function pedigree_update2(Request $request , $id){


           
      
        $image = $request->file('file_pedigree'.$id);
        $image_name=rand();
        $new_name = $image_name . '.' . $image->getClientOriginalExtension();
        $image->move(public_path('pdf_subidos'), $new_name);
        $url="/pdf_subidos"."/".$new_name ;
      

        Caballo::whereId($id)->update(array('pedigree' => "/pdf_subidos"."/".$new_name));  
  
        return redirect()->route('caballos.index');   
   

}
}


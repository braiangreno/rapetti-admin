<?php

namespace App\Http\Controllers;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use App\Models\Cambio;
use Illuminate\Support\Facades\DB;
use App\Models\Divisa;
use Illuminate\Support\Facades\Auth;

use App\Models\User;
use App\Models\RoleUser;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        $rol=RoleUser::whereId($user->id)->first();
      
        if($rol->role_id!=3)  {
            return redirect()->route('caballos.index');    
        }else{
            return redirect()->route('caballos.propietario');
        }
  
    }
    public function cambiar_referencias(Request $request) {



        return response()->json(['state' => 'success', 'data' => 'ok'], 200);
    }
    public function  emails(Request $request){

        $emails= DB::table('registro_email') ->orderBy('id', 'desc')->get();

        return view('emails.index',['emails'=>$emails]);
    }
}

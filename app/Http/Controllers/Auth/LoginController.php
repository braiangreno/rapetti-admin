<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Validations\ValidationsUser;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use App\Models\User;
use App\Models\RoleUser;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function login(Request $request)
    {

        $data = $request->all();
        $validator = ValidationsUser::login($data);
    
        if ($validator->fails())
        {
            \Session::flash('error', ['Credenciales invalidas']);
            return redirect()->back()->withInput($request->all());
        }

        $data['email'] = str_replace(' ', '', strtolower($request->get('email')));
      

  $user = User::whereEmail($data['email'])
            ->wherePassword($data['password'])->first();
        
        if (!is_null($user) || !empty($user))
        {
                Auth::loginUsingId($user->id, false);
                return Redirect::to('home');
        }
        

	Auth::logout();
        Session::flush();
        return redirect()->back()->withErrors(['errors' => 'Los datos no suministrados no coinciden']);
    }
}

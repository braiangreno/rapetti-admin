<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\RoleUser;
use App\Models\Foto;
use App\Models\FotoCaballo;
use App\Models\Video;
use App\Models\VideoCaballo;
use App\Models\Caballo;
use App\Validations\ValidationsUser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;

class ImagenesController extends Controller
{
    public function index()
    {        
  
    }

    public function register(Request $request) {
     
    }
    public function create(Request $request) {
   
        $validation = Validator::make($request->all(), [
            'file' => 'required|image|mimes:jpeg,png,jpg,gif|max:8048'
           ]);
    
           if($validation->passes())
           {
            $image = $request->file('file');
            $image_name=rand();
            $new_name = $image_name . '.' . $image->getClientOriginalExtension();
            $image->move(public_path('imagenes_subidas'), $new_name);
            $imagen= Foto::create([
                'nombre'=>$new_name,
                'url' =>"/imagenes_subidas"."/".$new_name
            ]);
            return response()->json([
             'message'   => 'Image Upload Successfully',
             'uploaded_image' => $new_name,
             'image_name'=>$image_name,
             'id'=> $imagen->id,
             'class_name'  => 'alert-success'
            ]);
           }
           else
           {
            return response()->json([
             'message'   => $validation->errors()->all(),
             'uploaded_image' => '',
             'class_name'  => 'alert-danger'
            ]);
           }
    }

    public function cargarimagenperfil(Request $request) {
   
        $image = $request->file('file');
        $image_name=rand();
        $new_name = $image_name . '.' . $image->getClientOriginalExtension();
        $image->move(public_path('imagenes_subidas'), $new_name);
        $imagen= Foto::create([
            'nombre'=>$new_name,
            'url' =>"/imagenes_subidas"."/".$new_name
        ]);

        $f=FotoCaballo::create([
            'tipo' => '2',
            'id_caballo'=>$request->input('id'),
            'id_foto'=>$imagen->id
        ]);
        Caballo::whereId($f->id_caballo)->update([
            'foto_perfil_id'=>$f->id_foto,
            'foto_perfil_url'=>$imagen->url
        ]);
   
        
        
    return back();
}
    public function cargarimagen(Request $request) {
   
        if($request->hasFile('file')){ 
            $image = $request->file('file');
            $image_name=rand();
            $new_name = $image_name . '.' . $image->getClientOriginalExtension();
            $image->move(public_path('imagenes_subidas'), $new_name);
            $imagen= Foto::create([
                'nombre'=>$new_name,
                'url' =>"/imagenes_subidas"."/".$new_name
            ]);

            $f=FotoCaballo::create([
                'tipo' => '1',
                'id_caballo'=>$request->input('id'),
                'id_foto'=>$imagen->id
            ]);
       
            
         }
           
            
        return back();
    }
    public function eliminarimagen(Request $request,$id){
      
 

        Foto::whereId($id)->delete();
        FotoCaballo::where('id_foto',$id)->delete();
        return back();
    }

    public function cargarvideo(Request $request) {
   
        $image = $request->file('file');
        $image_name=rand();
        $new_name = $image_name . '.' . $image->getClientOriginalExtension();
        $image->move(public_path('videos_subidas'), $new_name);
        $imagen= Video::create([
            'nombre'=>$new_name,
            'url' =>"/videos_subidas"."/".$new_name
        ]);

        $f=VideoCaballo::create([
            'tipo' => '1',
            'id_caballo'=>$request->input('id'),
            'id_video'=>$imagen->id
        ]);
   
        
        
          return back();
    }

    public function eliminarvideo(Request $request,$id){
    


        Video::whereId($id)->delete();
        VideoCaballo::where('id_video',$id)->delete();
        return back();
    }

    public function destroy(Request $request, $id) {
     
    }
}

<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\RoleUser;
use App\Validations\ValidationsUser;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function index()
    {        
        $usuarios = User::where('id','>','2')->orderBy('created_at', 'desc')->get();
        return view('usuarios.index',['usuarios'=>$usuarios]);
    }

    public function register(Request $request) {
        \DB::beginTransaction();
        try {

            $data = $request->all();
            $validator = ValidationsUser::store($data);

            if ($validator->fails()) {
                return response()->json(['msg' => 'fail', 'data' => $validator->errors()->first()], 200);
            }

            $usuario= User::create($data);
            $roles = [
                [
                    'user_id' => $usuario->id,
                    'role_id' => 3
                ],
    
            ];
    
            RoleUser::insert($roles);
        } catch (\Exception $e) {
            \DB::rollback();
            return response()->json(['msg' => 'fail', 'data' => $e->getMessage()], 200);
        }

        \DB::commit();
        return response()->json(['state' => 'success', 'data' => 'ok'], 200);
    }

    public function update(Request $request) {
        \DB::beginTransaction();
        try {

            $data = $request->all();
            $cliente = User::whereId($data['id'])->first();
            if($cliente->email == $data['email'])
            {
                unset($data['email']);
            }
            $validator = ValidationsUser::update($data);

            if ($validator->fails()) {
                return response()->json(['msg' => 'fail', 'data' => $validator->errors()->first()], 200);
            }

            unset($data['_token']);
            $id = $data['id'];

            User::whereId($id)->update($data);

        } catch (\Exception $e) {
            \DB::rollback();
            return response()->json(['msg' => 'fail', 'data' => $e->getMessage()], 200);
        }

        \DB::commit();
        return response()->json(['state' => 'success', 'data' => 'ok'], 200);
    }

    public function destroy(Request $request, $id) {
        \DB::beginTransaction();
        try {
            $request->request->add(['id'=>$id]);
            $data = $request->all();

            $validator = ValidationsUser::destroy($data);

            if ($validator->fails()) {
                return response()->json(['msg' => 'fail', 'data' => $validator->errors()->first()], 200);
            }

            User::whereId($id)->delete();

        } catch (\Exception $e) {
            \DB::rollback();
            return response()->json(['msg' => 'fail', 'data' => $e->getMessage()], 200);
        }

        \DB::commit();
        return response()->json(['state' => 'success', 'data' => 'ok'], 200);
    }
}

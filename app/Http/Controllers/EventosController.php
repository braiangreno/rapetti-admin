<?php

namespace App\Http\Controllers;
use DB; 
use App\Models\User;
use App\Models\RoleUser;
use App\Models\Foto;
use App\Models\FotoCaballo;
use App\Models\Video;
use App\Models\VideoCaballo;
use App\Models\Caballo;
use App\Validations\ValidationsUser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;

class EventosController extends Controller
{
    public function index()
    {        
  
    }

    public function crear() {
        $caballos = Caballo::where("state",1)->orderBy('created_at', 'desc')->get();

        return view('eventos.crear',['caballos'=>$caballos]);
    }
    public function crear_evento(Request $request) {
   
        $data_in= $request->all();

        switch( $data_in['tipo']){
            case 1:
                $id_evento=DB::table('event_peso')->insertGetId(
                    array('fecha'=>$data_in['fecha'], 'peso'=>$data_in['peso'])
                );
                DB::table('eventos')->insertGetId(
                    array(
                        'id_caballo'=>$data_in['caballo'],
                        'fecha'=>$data_in['fecha'],
                        'tipo'=>1,
                        'tipo_event_id'=>$id_evento,
                     )
                );
                Caballo::whereId($data_in['caballo'])->update([
                    'peso'=>$data_in['peso']
                ]);
           
            break;
                    
                  

            case 2: 
                 $data=[
                    'fecha'=>'',
                    'observaciones'=>'',
                    'foto_url'=>'',
                    'trabajo'=>'',
                ];/*
                if($request->hasFile('e2_video')){ 
                $image = $request->file('e2_file');
                $image_name=rand();
                $new_name = $image_name . '.' . $image->getClientOriginalExtension();
                $image->move(public_path('imagenes_subidas'), $new_name);
                    $data['foto_url']="/imagenes_subidas"."/".$new_name;
                }*/
              
                $data_in=$request->all();
                if(!empty($request->input('observaciones'))) {
                    $data['observaciones']=$data_in['observaciones'];
                }
                if(!empty($request->input('trabajo'))) {
                    $data['trabajo']=$data_in['trabajo'];
                }
    
                    $id_evento=DB::table('event_herraje')->insertGetId(
                    array(
                        'fecha'=>$data_in['fecha'],
                        'trabajo'=>$data['trabajo'],
                        'observaciones'=>$data['observaciones'],
                        'foto_url'=>'')
                );
          

                foreach($data_in['caballos'] as &$val)
                {

                    DB::table('eventos')->insertGetId(
                        array(
                            'id_caballo'=>$val,
                            'fecha'=>$data_in['fecha'],
                            'tipo'=>2,
                            'tipo_event_id'=>$id_evento,
                        )
                    );
                   
                }    
            break;

            case 3:
                
                            $validation = Validator::make($request->all(), [
                                'e3_file' => 'required|image|mimes:jpeg,png,jpg,gif|max:8048'
                            ]);
                            $url="";
                        if($validation->passes())
                        {
                            $image = $request->file('e3_file');
                            $image_name=rand();
                            $new_name = $image_name . '.' . $image->getClientOriginalExtension();
                            $image->move(public_path('imagenes_subidas'), $new_name);
                            $url="/imagenes_subidas"."/".$new_name ;
                        }
                            

                                $data=[
                                    'fecha'=>'',
                                        'ovizq'=>'',
                                        'ovder'=>'',
                                        'utero'=>'',
                                        'observaciones'=>'',
                                        'foto_url'=>'' ,
                                    
                                ];
                                $data_in=$request->all();
                                if(!empty($request->input('observaciones'))) {
                                    $data['observaciones']=$data_in['observaciones'];
                                }
                                if(!empty($request->input('ovizq'))) {
                                    $data['ovizq']=$data_in['ovizq'];
                                }
                                if(!empty($request->input('ovder'))) {
                                    $data['ovder']=$data_in['ovder'];
                                }
                                if(!empty($request->input('utero'))) {
                                    $data['utero']=$data_in['utero'];
                                }
                                $id_evento=DB::table('event_ecografia')->insertGetId(
                                    array(
                                        'fecha'=>$data_in['fecha'],
                                        'ovizq'=>$data['ovizq'],
                                        'ovder'=>$data['ovder'],
                                        'utero'=>$data['utero'],
                                        'observaciones'=>$data['observaciones'],
                                        'foto_url'=>$url
                                        )
                            );
                            

                                foreach($data_in['caballos'] as &$val)
                                {

                                    DB::table('eventos')->insertGetId(
                                        array(
                                            'id_caballo'=>$val,
                                                'fecha'=>$data_in['fecha'],
                                            'tipo'=>3,
                                            'tipo_event_id'=>$id_evento
                                        )
                                    );
                                
                                }     
            break;

            case 4:
                $validation = Validator::make($request->all(), [
                    'file' => 'required|image|mimes:jpeg,JPEG,png,PNG,jpg,JPG,gif,GIF|max:8048'
                   ]);
                    $id=0;
                   if($validation->passes())
                   {
                    $image = $request->file('file');
                    $image_name=rand();
                    $new_name = $image_name . '.' . $image->getClientOriginalExtension();
                    $image->move(public_path('imagenes_subidas'), $new_name);
                    $url="/imagenes_subidas"."/".$new_name ;
                 
                    $imagen= Foto::create([
                        'nombre'=>$new_name,
                        'url' =>"/imagenes_subidas"."/".$new_name
                    ]);
                    $id=$imagen->id;
                   }
                      
                   
                
            $data=[
                'fecha'=>'',
                'observaciones'=>'',
                'diagnostico'=>'',
                'fotos_id'=>$id,
                'video_url'=>'',
                'tratamiento'=>'',
                'droga'=>'',
            ];
            $data_in=$request->all();
      
            if(!empty($request->input('diagnostico'))) {
                $data['diagnostico']=$data_in['diagnostico'];
            }

            if(!empty($request->input('tratamiento'))) {
                $data['tratamiento']=$data_in['tratamiento'];
            }
            
   
            
            $data['fecha']=$data_in['fecha'];        
           
   
            if($request->hasFile('e4_video')){ 
                    $image2 = $request->file('e4_video');
                    $image_name2=rand();
                    $new_name2 = $image_name2 . '.' . $image2->getClientOriginalExtension();
                    $image2->move(public_path('videos_subidas'), $new_name2);
                    $data['video_url']="/videos_subidas"."/".$new_name2;
                  
                    
                 }
                 $id_evento=DB::table('event_veterinaria')->insertGetId($data);
             

                 DB::table('eventos')->insertGetId(
                                        array(
                                            'id_caballo'=>$data_in['caballo'],
                                            'fecha'=>$data['fecha'],
                                            'tipo'=>4,
                                            'tipo_event_id'=>$id_evento,
                                        )
                                    );
                         
            break;
            case 5:
                $data=[
                    'fecha'=>$data_in['fecha'],
                    'droga'=>$data_in['droga'],
                ];
                $id_evento=DB::table('event_parasitario')->insertGetId(
                    $data
                );
                foreach($data_in['caballos'] as &$val)
                {
                    DB::table('eventos')->insertGetId(
                        array(
                            'id_caballo'=>$val,
                            'fecha'=>$data_in['fecha'],
                            'tipo'=>5,
                            'tipo_event_id'=>$id_evento,
                         )
                    );
                }     
                         
            break;
            case 6:
                $image = $request->file('e4_file');
                $image_name=rand();
                $new_name = $image_name . '.' . $image->getClientOriginalExtension();
                $image->move(public_path('imagenes_subidas'), $new_name);

                $imagen= Foto::create([
                    'nombre'=>$new_name,
                    'url' =>"/imagenes_subidas"."/".$new_name
                ]);
                FotoCaballo::create([
                    'tipo' => '1',
                    'id_caballo'=>$data_in['caballo'],
                    'id_foto'=>$imagen->id
                ]);
                $data=[
                    'fecha'=>'',
                        'foto_url'=>'' ,
                ];
                $data_in=$request->all();
                $id_evento=DB::table('event_foto')->insertGetId(
                    array(
                        'fecha'=>$data_in['fecha'],
                        'foto_url'=>"/imagenes_subidas"."/".$new_name 
                        )
            );
            
        
                DB::table('eventos')->insertGetId(
                    array(
                        'id_caballo'=>$data_in['caballo'],
                             'fecha'=>$data_in['fecha'],
                        'tipo'=>6,
                        'tipo_event_id'=>$id_evento
                    )
                );
            break;
            case 7:
                $data=[
                    'fecha'=>$data_in['fecha'],
                    'vacuna'=>$data_in['vacunas'],
                ];
                $id_evento=DB::table('event_vacuna')->insertGetId(
                    $data
                );
                foreach($data_in['caballos'] as &$val)
                {
                    DB::table('eventos')->insertGetId(
                        array(
                            'id_caballo'=>$val,
                            'fecha'=>$data_in['fecha'],
                            'tipo'=>7,
                            'tipo_event_id'=>$id_evento,
                         )
                    );
                }     
                         
            break;
        }

        return Redirect::to('caballos/eventos?event='.$data_in['tipo']);
    }

    public function list($id) {
        $caballos = Caballo::whereId($id)->first();
        //  $caballos = Caballo::whereId($id)->orderBy('created_at', 'asc')->first();
        $eventos_caballo=DB::table('eventos')->where('id_caballo',$id)->get();

        return  view('caballos.eventos',['caballo'=>$caballos,'eventos'=>$eventos_caballo]);
    }
    
    public function eliminar($id) {
        $evento=DB::table('eventos')->where('id', $id)->first();      
        DB::table('eventos')->where('id', $id)->delete();/*
        switch($evento->tipo)
        {
            case 1:
                DB::table('event_peso')->where('id', $evento->tipo_event_id)->delete();
            break;
            case 2:
                DB::table('event_herraje')->where('id', $evento->tipo_event_id)->delete();
            break;
            case 3:
                DB::table('event_ecografia')->where('id', $evento->tipo_event_id)->delete();
            break;
            case 4:
                DB::table('event_veterinaria')->where('id', $evento->tipo_event_id)->delete();
            break;

        }*/
        return back();
    }
    public function detail($id) {
        $evento=DB::table('eventos')->where('id', $id)->first();      
        
        $table="";
        switch($evento->tipo)
        {
            case 1:
                $table="event_peso";
            break;
            case 2:
                $table="event_herraje";
            break;
            case 3:
                $table="event_ecografia";
            break;
            case 4:
                $table="event_veterinaria";
            break;
            case 5:
                $table="event_parasitario";
            break;
            case 6:
                $table="event_foto";
            break;
            case 7:
                $table="event_vacuna";
            break;
        }
        
        $evento_details=DB::table($table)->where('id', $evento->tipo_event_id)->first();
  
        
        switch($evento->tipo)
        {
            case 1:
                return view('eventos.'.$table,['e'=>$evento_details]);
            break;
            case 2:
                return view('eventos.'.$table,['e'=>$evento_details]);
            break;
            case 3:
                return view('eventos.'.$table,['e'=>$evento_details]);
            break;
            case 4:
                $fotos=Foto::where('id',$evento_details->fotos_id)->get();
           
                return view('eventos.'.$table,['e'=>$evento_details,'fotos'=>$fotos]);
            break;
            case 5:
                return view('eventos.'.$table,['e'=>$evento_details]);
            break;
            case 6:
                return view('eventos.'.$table,['e'=>$evento_details]);
            break;
            case 7:
                return view('eventos.'.$table,['e'=>$evento_details]);
            break;
        }
     
    }
    
    public function destroy(Request $request, $id) {
     
    }
    public function historial(Request $request, $id){

        $eventos_eco=DB::table('eventos')->where('id_caballo',$id)->where('tipo','3')->get();
       
        $event_id=array();
      
        foreach($eventos_eco as $ev){

            $event_id[]=$ev->tipo_event_id;
        }
        $eventos = DB::table('event_ecografia')->whereIn('id',$event_id)->get();
        return view('eventos.historial_reproductivo',['eventos'=>$eventos]);

    }


    public function peso_caballo_update(Request $request, $id){
        $data_in = $request->all();
        
        Caballo::whereId($id)->update($data_in);    
        return ;
    }

    public function editar(Request $request, $id){
        $evento=DB::table('eventos')->where('id', $id)->first();      
        $table="event_ecografia";
        $evento_details=DB::table($table)->where('id', $evento->tipo_event_id)->first();
        return view('eventos.historial_reproductivo_edit',['e'=>$evento_details]);
    }

    public function editar_update(Request $request, $id){
       
        $data_in=$request->all();  
        $table="event_ecografia";
        $evento=DB::table('eventos')->where('tipo_event_id', $id)->first();   
        
        if($request->file('e3_file')){

            $url="";
            $new_name = '';
            
        $validation = Validator::make($request->all(), [
            'e3_file' => 'image|mimes:jpeg,png,jpg,gif'
        ]);

        $custom_validation_messages = array(
           // 'e3_file.required' => "El nombre es requerido.",
            'e3_file.image' => "El archivo debe ser de tipo imagen",
            'e3_file.mimes' => "La imagen debe tener formato jpeg,png,jpg,gif",
        //    'e3_file.max' => "La imagen debe tener un tamaño máximo de 8048",
       //     'email.unique' => "El email ya se encuentra registrado.",
          );

          $validator = Validator::make($request->all(), [
            'e3_file' => 'image|mimes:jpeg,png,jpg,gif' 
  
        ],$custom_validation_messages);
 
        if ($validator->fails()) {
            return back()
                   ->withErrors($validator)
                   ->withInput();
        }

       if($validation->passes())
       {
        $image = $request->file('e3_file');
        $image_name=rand();
        $new_name = $image_name . '.' . $image->getClientOriginalExtension();
        $image->move(public_path('imagenes_subidas'), $new_name);
        $url="/imagenes_subidas"."/".$new_name ;
       }else{ 
           return back();
       }

    }
       
         
        DB::table($table)->where('id', $id)->update(
            array(
                'fecha'=>$data_in['fecha'],
                'ovizq'=>$data_in['ovizq'],
                'ovder'=>$data_in['ovder'],
                'utero'=>$data_in['utero'],
                'img_ecografia' => $new_name,
                'observaciones'=>$data_in['observaciones'],
                )
    );
    
    return Redirect::to('caballos/eventos/'.$evento->id_caballo);
    }

    public function eventos_lista(Request $request,$caballo, $id){
        

        switch($id)
        {
            case 2:
                $table="event_herraje";
            break;
            case 3:
                $table="event_ecografia";
            break;
            case 4:
                $table="event_veterinaria";
            break;
            case 5:
                $table="event_parasitario";
            break;
            case 7:
                $table="event_vacuna";
            break;
        }
        $evento=DB::table('eventos')->where('id_caballo',$caballo)->where('tipo', $id)->get();   
 
        $ids=[];
        foreach($evento as $e){     
            $ids[]=(int)$e->tipo_event_id;
        }
   
        $evento_details=DB::table($table)->whereIn('id',$ids)->get();
   
        if($id==4){
             $fotos=[];
            foreach($evento_details as $e){
                $fotos[$e->id]=Foto::where('id',$e->fotos_id)->get();
            }
   
        return view('eventos.lista_eventos',['eventos'=>$evento_details,'evento_type'=>$id,'fotos'=>$fotos]);
        }
        return view('eventos.lista_eventos',['eventos'=>$evento_details,'evento_type'=>$id]);
       
    }

}

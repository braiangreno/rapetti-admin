<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/',['as' => 'web.login', 'uses' => 'Auth\LoginController@showLoginForm']);
Route::post('/',['as' => 'login', 'uses' => 'Auth\LoginController@login']);
Route::post('/logout',['as' => 'logout', 'uses' => 'Auth\LoginController@logout']);
Route::get('/login',['as' => 'web.login', 'uses' => 'Auth\LoginController@showLoginForm']);
Route::post('/login',['as' => 'login', 'uses' => 'Auth\LoginController@login']);
// Password Reset Routes...
$this->get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm');
$this->post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail');
$this->get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm');
$this->post('password/reset', 'Auth\ResetPasswordController@reset');


Route::group(['middleware' => ['web', 'authenticate']], function ()
{
    Route::get('/home', 'HomeController@index')->name('home');
    Route::post('/cambiar_referencias',['as' => 'cambiar_referencias', 'uses' => 'HomeController@cambiar_referencias']);
    Route::post('/imagenes/upload', ['as' => 'imagenes_upload', 'uses' => 'ImagenesController@create']);
    Route::post('/imagenes_consulta/upload', ['as' => 'imagenes_upload_consulta', 'uses' => 'ImagenesController@create2']);
    Route::group(['prefix' => 'users'], function()
    {
        Route::get('/', ['as' => 'users.index', 'uses' => 'UserController@index']);
        Route::post('/register', ['as' => 'users.register', 'uses' => 'UserController@register']);
        Route::post('/update', ['as' => 'users.update', 'uses' => 'UserController@update']);
        Route::get('/eliminar/{id}', ['as' => 'users.delete', 'uses' => 'UserController@destroy']);
    });
    Route::post('/caballos_pedigree_up/{id}', ['as' => 'caballo_pedigree_update', 'uses' => 'CaballosController@pedigree_update']); 
    Route::post('/caballos_pedigree_up2/{id}', ['as' => 'caballo_pedigree_update2', 'uses' => 'CaballosController@pedigree_update2']); 
    Route::get('/emails', ['as' => 'emails.index', 'uses' => 'HomeController@emails']);
    Route::group(['prefix' => 'caballos'], function()
    {
        Route::get('/', ['as' => 'caballos.index', 'uses' => 'CaballosController@index']);
        Route::get('/crear', ['as' => 'caballos.create', 'uses' => 'CaballosController@create']);
        Route::get('/perfil/adm/{id}', ['as' => 'caballos_profile', 'uses' => 'CaballosController@perfil']);
        Route::get('/activar', ['as' => 'caballos_activar', 'uses' => 'CaballosController@activar']);
        Route::get('/alimentacion', ['as' => 'caballos_alimentacion', 'uses' => 'CaballosController@alimentacion']);
        Route::post('/alimentacion', ['as' => 'caballos_alimentacion2', 'uses' => 'CaballosController@alimentacionchange']);
        Route::get('/pesos', ['as' => 'caballos_caballo', 'uses' => 'CaballosController@pesos_potrillo']); 
        Route::get('/pesos_general', ['as' => 'pesos_general', 'uses' => 'CaballosController@pesos_tabla']); 
        Route::post('/pesos_general', ['as' => 'pesos_general2', 'uses' => 'CaballosController@pesos_tabla_update']); 
        Route::post('/peso_update/{id}', ['as' => 'peso_caballo_update', 'uses' => 'EventosController@peso_caballo_update']);       
        Route::post('/pesos', ['as' => 'caballos_caballo2', 'uses' => 'CaballosController@pesos_potrillo_update']);  
        
       
        Route::post('/pesosget/{id}', ['as' => 'caballos_caballo3', 'uses' => 'CaballosController@pesos_potrillo_get']);           
        Route::get('/imagenes/{id}', ['as' => 'caballos_imagenes', 'uses' => 'CaballosController@imagenes']);
        Route::post('/cargarimagen', ['as' => 'caballos_imagenes_upload', 'uses' => 'ImagenesController@cargarimagen']);
        Route::post('/cargarimagenperfil', ['as' => 'caballos_imagenes_upload_perfil', 'uses' => 'ImagenesController@cargarimagenperfil']);
        Route::post('/cargarvideo', ['as' => 'caballos_video_upload', 'uses' => 'ImagenesController@cargarvideo']);
        Route::get('/imagenes/eliminar/{id}', ['as' => 'caballos_imagenes_eliminar', 'uses' => 'ImagenesController@eliminarimagen']);
        Route::get('/videos/eliminar/{id}', ['as' => 'caballos_videos_eliminar', 'uses' => 'ImagenesController@eliminarvideo']);
        Route::get('/videos/{id}', ['as' => 'caballos_videos', 'uses' => 'CaballosController@videos']);
        Route::get('/videos_prop/{id}', ['as' => 'caballos_videos', 'uses' => 'CaballosController@videos2']);
        Route::get('/eventos/{id}', ['as' => 'caballos_eventos', 'uses' => 'CaballosController@eventos']);
        Route::post('/create', ['as' => 'caballos.register', 'uses' => 'CaballosController@register']);
        Route::post('/update', ['as' => 'caballos.update', 'uses' => 'CaballosController@update']);
        Route::get('/eliminar/{id}', ['as' => 'caballos.delete', 'uses' => 'CaballosController@destroy']);

        Route::get('/propietarios/{id}', ['as' => 'propietarios_caballos', 'uses' => 'CaballosController@propietario_caballo']);
        Route::get('/evento/editar/{id}', ['as' => 'caballos.editar.eventos', 'uses' => 'EventosController@editar']);
        Route::post('/evento/editar/{id}', ['as' => 'caballos.editar.eventos', 'uses' => 'EventosController@editar_update']);
        Route::get('/eventos', ['as' => 'caballos.eventos', 'uses' => 'EventosController@crear']);
        Route::get('/eventos_lista/{caballo}/{id}', ['as' => 'caballos.eventos_lista', 'uses' => 'EventosController@eventos_lista']);
        Route::get('/eventos/{id}', ['as' => 'caballos.eventos.mirar', 'uses' => 'EventosController@list']);
        Route::get('/evento/eliminar/{id}', ['as' => 'caballos.eventos.eliminar', 'uses' => 'EventosController@eliminar']);
        Route::get('/evento/detalle/{id}', ['as' => 'caballos.eventos.detail', 'uses' => 'EventosController@detail']);
        Route::get('/historial-reproductivo/{id}', ['as' => 'caballos_profile_prop', 'uses' => 'EventosController@historial']);

        Route::get('/propietario', ['as' => 'caballos.propietario', 'uses' => 'CaballosController@propietario']);
        Route::get('/perfil/prop/{id}', ['as' => 'caballos_profile_prop', 'uses' => 'CaballosController@perfil_prop']);

      



    });

    Route::post('/eventos/create', ['as' => 'eventos_crear', 'uses' => 'EventosController@crear_evento']);
      
    
   
  


});


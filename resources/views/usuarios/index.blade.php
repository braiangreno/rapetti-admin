<?php /**
 * Created by PhpStorm.
 * User: raffa
 * Date: 23/9/2018
 * Time: 2:35 PM
 */ ?>
@extends('adminlte::page')

@section('title', 'Propietarios - Haras - Rapetti')

@section('content_header')
    <h1>Propietarios <button type="button" id="agregar_cliente" class="btn btn-block btn-success btn" style="    width: auto;    display: inline;float:right;">Agregar +</button>
    </h1>
        @stop

@section('content')

    <div class="col-xs-12 col-sm-12 col-md-8 col-lg-6 " id="crear-nuevo-cliente" style="display:none;">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Ingresar nuevo propietario </h3>
                    <button type="button " class="btn pull-right" id="cerrar-nuevo-cliente">Cerrar</button>
                </div>
                <form id="form_crear" method="post">
                    {{ csrf_field() }}
                    <div class="box-body">
                        <div class="row">
                            <div class="col-xs-12 col-sm-4 col-md-4 ">
                                    <label for="Nombre">Nombre</label>
                                    <input type="text" class="form-control required"  required name="Nombre" id="Nombre" placeholder="Nombre">
                            </div>
                            <div class="col-xs-12 col-sm-4 col-md-4 ">
                                    <label for="Apellido">Apellido</label>
                                    <input type="text" class="form-control required"  required name="Apellido" id="Apellido" placeholder="Apellido">
                            </div>
                            <div class="col-xs-12 col-sm-4 col-md-4">
                                    <label for="Telefono">Telefono</label>
                                    <input type="number" class="form-control input-numbe required" required name="Telefono" id="Telefono" placeholder="Telefono">
                            </div>
                            <div class="col-xs-12 col-sm-4 col-md-4">
                                    <label for="Email">Email</label>
                                    <input type="email" class="form-control required" required name="Email" id="Email" placeholder="Dirección de Correo">
                            </div>
                            <div class="col-xs-12 col-sm-4 col-md-4">
                                    <label for="Password">Contraseña</label>
                                    <input type="password" class="form-control required" required name="Password" id="Password" placeholder="Contraseña">
                            </div>
                            <div class="col-xs-12   col-sm-4 col-md-4 pull-right">
                                <label>&nbsp; </label>
                                <button class="btn btn-block btn-primary" id="crear_button">Crear propietario <i class="fa fa-refresh spin" style="display:none" id="cargando_form"> </i></button>
                            </div>

                        </div>
                        <div class="form-group has-error">
                            <label class="control-label" for="inputError"><i class="fa fa-times-circle-o" id="mensaje-error"></i>
                            </label>

                        </div>

                    </div><!-- /.box-body -->
                </form>
            </div><!-- /.box -->
        </div>
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="box">
            <div class="box-header">
                Propietarios
            </div>
            <div class="box-body table-responsive">
                <table id="usuarios" class="table table-bordered ">
                    <thead>
                    <tr>
                        <th>Nru</th>
                        <th>Nombre</th>
                        <th>Email</th>
                        <th>Telefono</th>
                        <th>Opciones</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($usuarios as $c)
                        <tr>
                            <td>{{$c->id}}</td>
                            <td>{{$c->name." ". $c->lastname }}</td>
                            <td>{{$c->email}}</td>
                            <td>{{$c->telefono}}</td>
                            <td>
                                 <a href="#"
                                   data-id="{{$c->id}}"
                                   data-nombre="{{$c->name}}"
                                   data-apellido="{{$c->lastname}}"
                                   data-telefono="{{$c->telefono}}"
                                   data-email="{{$c->email}}"
                                   data-password="{{$c->password}}"
                                   data-toggle="modal"
                                   data-target="#modalEdit"
                                   data-skin="skin-blue" title="Editar" class="btn btn-primary btn-xs modal-opening-edit"><i class="fa fa-edit"></i></a>
                                <a href="#" data-skin="skin-red"
                                   data-id="{{$c->id}}"
                                   data-nombre="{{$c->name}}"
                                   data-toggle="modal"
                                   data-target="#modalDelete"
                                   title="Eliminar" class="btn btn-danger btn-xs modal-opening-delete"><i class="fa fa-remove"></i></a>
                            </td>
                        </tr>
                    @endforeach

                    </tbody>
                </table>
            </div>

        </div>
    </div>



    <div id="modalInfo" class="modal">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="titulo-modal-info"></h4>
                    </div>
                    <div class="modal-body">


                                <table class="table table-condensed">
                                    <tr>
                                        <td><strong>Nombre</strong></td>
                                        <td id="modal-info-nombre">

                                        </td>
                                    </tr>
                                    <tr>
                                        <td><strong>Telefono</strong></td>
                                        <td id="modal-info-telefono">

                                        </td>
                                    </tr>
                                    <tr>
                                        <td><strong>Direccion</strong></td>
                                        <td id="modal-info-direccion">

                                        </td>
                                    </tr>
                                    <tr>
                                        <td><strong>Email</strong></td>
                                        <td id="modal-info-email">

                                        </td>
                                    </tr>
                                    <tr>
                                        <td><strong>Contacto</strong></td>
                                        <td id="modal-info-contacto">

                                        </td>
                                    </tr>


                                </table>




                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn pull-left" data-dismiss="modal">Cerrar</button>

                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->

    <div id="modalEdit" class="modal modal-primary">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="titulo-modal-edit"></h4>
                </div>
                <form id="form_edit" method="post">
                <div class="modal-body">

                    <input  type="hidden" name="modal_edit_id" id="modal_edit_id">
                    <table class="table table-condensed">
                        <tr>
                            <td><strong>Nombre</strong></td>
                            <td>
                                <input type="text" class="form-control required"  required name="modal_edit_nombre" id="modal_edit_Nombre" >
                            </td>
                        </tr>
                        <tr>
                            <td><strong>Apellido</strong></td>
                            <td>
                                <input type="text" class="form-control required"  required name="modal_edit_Apellido" id="modal_edit_Apellido" >
                            </td>
                        </tr>
                        <tr>
                            <td><strong>Telefono</strong></td>
                            <td>
                                <input type="text" class="form-control required"  required name="modal_edit_Telefono" id="modal_edit_Telefono" >
                            </td>
                        </tr>
                      
                        <tr>
                            <td><strong>Email</strong></td>
                            <td >
                                <input type="text" class="form-control required"  required name="modal_edit_Email" id="modal_edit_Email" >

                            </td>
                        </tr>
                        <tr>
                            <td><strong>Password</strong></td>
                            <td>
                                <input type="password" class="form-control required"  required name="modal_edit_Password" id="modal_edit_Password">
                            </td>
                        </tr>
                        <tr>
                            <div class="form-group has-error">
                                <label class="control-label" for="inputError"><i class="fa fa-times-circle-o" id="mensaje-error-edit"></i>
                                </label>

                            </div>
                        </tr>

                    </table>
                </div>
                <div class="modal-footer">
                    <button type="button " class="btn   btn-outline pull-left" data-dismiss="modal">Cerrar</button>
                    <button type="submit" class="btn btn-outline">Actualizar<i class="fa fa-refresh spin" style="display:none" id="cargando_form_edit"> </i></button>

                </div>
                </form>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

    <div id="modalDelete" class="modal modal-danger">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="titulo-modal-delete"></h4>
                </div>
                <form id="form_delete" method="post">
                    <div class="modal-footer">
                        <input type="hidden" id="modal_delete_id" name="modal_delete_id">
                        <button type="button " class="btn   btn-outline pull-left" data-dismiss="modal">CANCELAR</button>
                        <button type="submit" class="btn  btn-success">CONFIRMAR</button>

                    </div>
                </form>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->



@stop

@section("js")
    <script>
        $("#crear-nuevo-cliente").hide();
        $("#mensaje-error").hide();
        $("#mensaje-error-edit").hide();
        $('.modal-opening-info').on('click', function (e) {
            var modal_id = $(this).data('id');
            var modal_apellido= $(this).data('apellido');
            var modal_telefono = $(this).data('telefono');
            var modal_password = $(this).data('password');
            var modal_nombre = $(this).data('nombre');
            var modal_email = $(this).data('email');
            $("#titulo-modal-info").html("Cliente ID# "+modal_id);
            $("#modal-info-apellido").html(modal_apellido);
            $("#modal-info-nombre").html(modal_nombre);
            $("#modal-info-password").html(modal_direccion);
            $("#modal-info-telefono").html(modal_telefono);
            $("#modal-info-email").html(modal_email);

        });

        $('#modalEdit').on('hide.bs.modal', function (e) {
            $("#modal_edit_id").val('');
            $("#modal_edit_Nombre").val('');
            $("#modal_edit_Email").val('');
            $("#modal_edit_Telefono").val('');
            $("#modal_edit_Apellido").val('');
            $("#modal_edit_Password").val('');
        });

        $('#modalDelete').on('hide.bs.modal', function (e) {
            $("#modal_delete_id").val('');
        });


        $('.modal-opening-edit').on('click', function (e) {
            var modal_id = $(this).data('id');
            var modal_apellido = $(this).data('apellido');
            var modal_telefono = $(this).data('telefono');
            var modal_password = $(this).data('password');
            var modal_nombre = $(this).data('nombre');
            var modal_email = $(this).data('email');
            console.log(modal_email);

            $("#titulo-modal-edit").html("Cliente ID# "+modal_id);
            $("#modal_edit_Password").val(modal_password);
            $("#modal_edit_Nombre").val(modal_nombre);
            $("#modal_edit_Apellido").val(modal_apellido);
            $("#modal_edit_Telefono").val(modal_telefono);
            $("#modal_edit_Email").val(modal_email);
            $("#modal_edit_id").val(modal_id);

        })


        $('.modal-opening-delete').on('click', function (e) {
            var modal_id = $(this).data('id');
            var modal_nombre = $(this).data('nombre');
            console.log(modal_nombre);

            $("#titulo-modal-delete").html("¿ Seguro desea eliminar el cliente #"+modal_id+" : "+modal_nombre +"?");

            $("#modal_delete_id").val(modal_id);

        })
        $( document ).ready(function() {

            $('#clientes').DataTable( {
                "language":{
                    "sProcessing":     "Procesando...",
                    "sLengthMenu":     "Mostrar _MENU_ registros",
                    "sZeroRecords":    "No se encontraron resultados",
                    "sEmptyTable":     "Ningún dato disponible en esta tabla",
                    "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                    "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
                    "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                    "sInfoPostFix":    "",
                    "sSearch":         "Buscar:",
                    "sUrl":            "",
                    "sInfoThousands":  ",",
                    "sLoadingRecords": "Cargando...",
                    "oPaginate": {
                        "sFirst":    "Primero",
                        "sLast":     "Último",
                        "sNext":     "Siguiente",
                        "sPrevious": "Anterior"
                    },
                    "oAria": {
                        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                    }
                }
            } );
            var clickeado=0;
            var token = $('#_token').val();
            $('#agregar_cliente').click(function(e) {
                if(clickeado==0){
                    $("#crear-nuevo-cliente").slideToggle()
                    clickeado++;
                }

            });

            $('#cerrar-nuevo-cliente').click(function(e) {
                if(clickeado>0){
                    $("#crear-nuevo-cliente").slideToggle()
                    clickeado=0;
                }

            });



            $('#form_crear').submit(function(ev) {
                    ev.preventDefault();

                    var route = "/users/register";
                   $("#cargando_form").show();
                    var Nombre = $("#Nombre").val();
                    var Apellido = $("#Apellido").val();
                    var Password = $("#Password").val();
                    var Email = $("#Email").val();
                    var Telefono = $("#Telefono").val();
                    $("#Nombre").prop('disabled', true);
                    $("#Apellido").prop('disabled', true);
                    $("#Password").prop('disabled', true);
                    $("#Email").prop('disabled', true);
                    $("#Telefono").prop('disabled', true);
                $.ajax({
                    url: route,
                    headers: {  'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')},
                    type: 'POST',
                    data: {
                        '_token': $('meta[name="_token"]').attr('content'),
                        'name': Nombre,
                        'lastname':Apellido,
                        'password':Password,
                        'email': Email,
                        'telefono': Telefono,
                    },
                    success: function(result)
                    {

                        if(result != 'null' && result.msg!='fail'){

                            setTimeout(
                                    function()
                                    {
                           
                                        $("#Nombre").val('');
                                        $("#Apellido").val('');
                                        $("#Email").val('');
                                        $("#Telefono").val('');
                                        $("#Password").val('');
                                        $("#Apellido").prop('disabled', false);
                                        $("#Password").prop('disabled', false);
                                        $("#Nombre").prop('disabled', false);
                                        $("#Email").prop('disabled', false);
                                        $("#Telefono").prop('disabled', false);


                                            $("#cargando_form").hide();
                                            $("#mensaje-error").hide();
                                            $('#modalEdit').modal('hide');
                                            location.reload();


                                    }, 1200);
                        }else{
                            $("#cargando_form_edit").hide();
                            $("#mensaje-error").html(" "+result.data);
                            $("#mensaje-error").show();
                            $("#Apellido").prop('disabled', false);
                            $("#Password").prop('disabled', false);
                            $("#Nombre").prop('disabled', false);
                            $("#Email").prop('disabled', false);
                            $("#Telefono").prop('disabled', false);

                        }

                    }

                });
                      




                  return false;
            });

            ///////////////////

            $('#form_edit').submit(function(ev) {
                ev.preventDefault();
                var route = "/users/update";
                $("#cargando_form_edit").show();
                var Id=$("#modal_edit_id").val();
                var Nombre = $("#modal_edit_Nombre").val();
                var Apellido = $("#modal_edit_Apellido").val();
                var Email = $("#modal_edit_Email").val();
                var Telefono = $("#modal_edit_Telefono").val();
                var Password = $("#modal_edit_Password").val();
                $("#modal_edit_Nombre").prop('disabled', true);;
                $("#modal_edit_Apellido").prop('disabled', true);;
                $("#modal_edit_Email").prop('disabled', true);
                $("#modal_edit_Telefono").prop('disabled', true);
                $("#modal_edit_Password").prop('disabled', true);
                $.ajax({
                    url: route,
                    headers: {'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')},
                    type: 'POST',
                    data: {
                        'id':Id,
                        'name': Nombre,
                        'lastname':Apellido,
                        'email': Email,
                        'telefono': Telefono,
                        'password':Password
                    },
                    success: function(result)
                    {

                        if(result != 'null' && result.msg!='fail'){

                            setTimeout(
                                    function()
                                    {
                                        $("#modal_edit_id").val('');
                                        $("#modal_edit_Nombre").val('');
                                        $("#modal_edit_Apellido").val('');
                                        $("#modal_edit_Email").val('');
                                        $("#modal_edit_Telefono").val('');
                                        $("#modal_edit_Password").val('');
                                        $("#modal_edit_Nombre").prop('disabled', false);
                                        $("#modal_edit_Apellido").prop('disabled', false);
                                        $("#modal_edit_Email").prop('disabled', false);
                                        $("#modal_edit_Telefono").prop('disabled', false);
                                        $("#modal_edit_Password").prop('disabled', false);

                                            $("#cargando_form_edit").hide();
                                            $("#mensaje-error").hide();
                                            $('#modalEdit').modal('hide');
                                            location.reload();


                                    }, 1200);
                        }else{
                            $("#cargando_form_edit").hide();
                            $("#mensaje-error-edit").html(" "+result.data);
                            $("#mensaje-error-edit").show();
                            $("#modal_edit_Nombre").prop('disabled', false);
                            $("#modal_edit_Apellido").prop('disabled', false);
                            $("#modal_edit_Email").prop('disabled', false);
                            $("#modal_edit_Telefono").prop('disabled', false);
                            $("#modal_edit_Password").prop('disabled', false);

                        }

                    }

                });
                return false;
            });

            $('#form_delete').submit(function(ev) {
                ev.preventDefault();

                $("#cargando_form_edit").show();
                var Id=$("#modal_delete_id").val();
                console.log(Id);
                var route = "users/eliminar/"+Id+"/";
                $.ajax({
                    url: route,
                    headers: {'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')},
                    type: 'POST',
                    data: {
                        'id':Id
                    },
                    success: function(result)
                    {

                        if(result != 'null' && result.msg!='fail'){

                            setTimeout(
                                    function()
                                    {
                                        $("#modal_delete_id").val('');
                                        $('#modalDelete').modal('hide');
                                      location.reload();
                                    }, 1200);
                        }

                    }

                });
                return false;
            });






        });
    </script>

@stop
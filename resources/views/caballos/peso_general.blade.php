


<?php /**
 * Created by PhpStorm.
 * User: raffa
 * Date: 23/9/2018
 * Time: 2:35 PM
 */ ?>
@extends('adminlte::page')

@section('title', 'Caballos - Haras - Rapetti')

@section('content_header')
    <h1>Pesos Tabla General  <a class="btn btn-success" href="/caballos">< Lista de Caballos </a></h1>
@stop

@section('content')

<form  method="post" action="{{url('caballos/pesos_general')}}" enctype="multipart/form-data">         {!! csrf_field() !!}
            
    <div class="row">
        <div class="col-xs-12 col-md-6 col-lg-6">
        <h3>PESO MIN</h3>
        <ul style=" list-style-type: none;">

            @foreach($pesos_min as $p)
            <li>
                <div class="form-group">
                    <label for="mes_{{$p->mes}}_{{$p->tipo}}" class="min-width: 50px!important;">MES {{$p->mes}}</label>
                    <input   name="mes_{{$p->mes}}_{{$p->tipo}}" type="number" step="0.01" id="mes_{{$p->mes}}_{{$p->tipo}}" value="{{$p->peso}}">
                </div>
            </li>
            @endforeach

        </ul>

        </div>
        <div class="col-xs-12 col-md-6 col-lg-6">
        <h3>PESO MAX</h3>
        <ul style=" list-style-type: none;">

            @foreach($pesos_max as $p)
            <li>
                <div class="form-group">
                    <label for="mes_{{$p->mes}}_{{$p->tipo}}" class="min-width: 50px!important;">MES {{$p->mes}}</label>
                    <input   name="mes_{{$p->mes}}_{{$p->tipo}}" type="number" step="0.01" id="mes_{{$p->mes}}_{{$p->tipo}}" value="{{$p->peso}}">
                </div>
            </li>
            @endforeach

        </ul>

        </div>
    </div>
    <button type="submit" class="btn btn-info"> Actualizar </button>
</form>


@stop

@section("js")

@stop
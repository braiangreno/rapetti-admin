<?php /**
 * Created by PhpStorm.
 * User: raffa
 * Date: 23/9/2018
 * Time: 2:35 PM
 */ ?>
@extends('adminlte::page')

@section('title', 'Caballos - Haras - Rapetti')

@section('content_header')
    <h1>Perfil Caballo  <a class="btn btn-success" href="/caballos">< Lista de Caballos </a></h1>
@stop

@section('content')
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.bundle.min.js"></script>

<div class="row">
    <div class="box">
        <div class="col-md-3">

            <!-- Profile Image -->
            <div class="box box-primary">
                <div class="box-body box-profile">
                @if($caballo->foto_perfil_id==0)
                    <img class="profile-user-img img-responsive img-circle" src="/imagenes_subidas/empty.png" alt="User profile picture">
                @else
                       <img class="profile-user-img img-responsive img-circle" style="width:100px;height:100px" src="{{$caballo->foto_perfil_url}}" alt="User profile picture">
       
                @endif
           
                <form  id="pedigree" method="post" action="/caballos_pedigree_up/{{$id}}" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <input class="form-control" type="file" id="file_pedigree" name="file_pedigree" style="opacity:0;height:0;"/>
                    <input type="hidden" name="id" value="{{$id}}">
                  
                    <input type="button" id="upload_pedigree" hidden class="btn hidden btn-primary btn-block">
                </form>
                <button  class="btn btn-info"  style=" 
                        position: absolute;
                        top: 10px;
                        right: 85px;
                        font-size: 12px;" onclick="thisFileUpload();" title="Cargar pedigree"
                    > <i class="fas fa-file-import"></i></button> 
                @if($caballo->pedigree!='')

                <a class="btn btn-success" style="position: absolute;
                                    top: 10px;
                                    right: 10px;font-size: 12px;" target="_blank" href="{{$caballo->pedigree}}"> Pedigree</a>
                @endif
              
                <form  id="foto-perfil" method="post"  action="/caballos/cargarimagenperfil" enctype="multipart/form-data">
                {{ csrf_field() }}
                <input class="form-control" type="file" id="file" name="file" style="opacity:0;height:0;"/>
                <h3 class="profile-username text-center" >{{$caballo->nombre}}</h3>
                <input type="hidden" name="id" value="{{$id}}">
                <input type="button" id="upload" class="btn btn-primary btn-block" value="Cambiar Imagen">
                </form>
                <ul class="list-group list-group-unbordered">
                <li class="list-group-item">
                         <b>Fecha de nacimiento</b> <span class="pull-right">{{$caballo->fecha_de_nacimiento}}</span>
                    </li>
                    <li class="list-group-item">
                    <b>Fecha de ingreso</b> <span class="pull-right">{{$caballo->fecha_de_ingreso}}</span>
                    </li>
                    <li class="list-group-item">
                         <b>Sexo</b> <span class="pull-right">{{$caballo->sexo}}</span>
                    </li>
                    <li class="list-group-item">
                         <b>Peso</b> <span class="pull-right">{{$caballo->peso}}</span>
                    </li>
                    <li class="list-group-item">
                          <b>Pelo</b> <span class="pull-right">{{$caballo->pelo}}</span>
                    </li>
                    <li class="list-group-item">
                          <b>Padre</b> <span class="pull-right">{{$caballo->padre}}</span>
                    </li>
                    <li class="list-group-item">
                         <b>Madre</b> <span class="pull-right">{{$caballo->madre}}</span>
                    </li>
                    <li class="list-group-item">
                    <b>Alimentación</b><br>
                    @switch($caballo->alimentacion)
                                @case(1)
                                    06.00 3kg avena<br>
                                    17:00 3kg avena  <br>                              
                                    <br>
                                    Pastoreo durante todo el día.<br>

                                    De noche 8kg alfalfa seca
                                @break

                                @case(2)

                                    Racion Puro Trato Potro Inicial (1,5% del peso vivo) en dos comidas diarias<br>
                                    Hemolitan 5cc por dia<br>
                                    Organew 5g por día<br>
                                    Aminomix 20g-40g por dia<br>
                                    <br>
                                    Pastoreo durante todo el día.<br>
                                    De noche 3-5 kg alfalfa.<br>
                                @break

                                @case(3)
                                    Racion Puro Trato Potro Final (1,5% del peso vivo) en dos comidas diarias<br>
                                    Organew 5g por día<br>
                                    Aminomix 50g por dia<br>
                                    Glicopan 40cc por dia<br>
                                    <br>
                                    25 kg de alfalfa (verde y seca mezclada) por día.<br>
                                @break

                                @case(4)
                                    06.00 3kg avena <br>
                                    17:00 3kg avena<br>
                                    <br>
                                    25 kg de alfalfa (verde y seca mezclada) por día.<br>
                                @break
                                @case(0)
                                    Alimentación no asignada.
                                @break
                         
                            @endswitch              
                    </li>
                    <li class="list-group-item">
                    <strong><i class="fa fa-comments margin-r-5"></i> Observaciones</strong>

                            <p class="text-muted">

                             {!! nl2br ($caballo->observaciones)!!}
                            </p>


                    </li>
                </ul>

                
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->

        </div>
       @if(count($pesos)>0)
        <div class="col-md-6">
                <!-- About Me Box -->
                <div class="box box-primary">
                <div class="box-header with-border">
                <h3 class="box-title"></h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">

                      <div id="chartContainer" style="height: 370px; width: 100%;"></div>
                <hr>

                </div>
                <!-- /.box-body -->
            </div>
            <script>
                    window.onload = function () {
                        var chart = new CanvasJS.Chart("chartContainer", {
                      
                        title:{
                            text: "Control de crecimiento"
                        },
                        axisY:[{
                            title: "Peso",
                            lineColor: "#000",
                
                            labelFontColor: "BLUE",
                            interval: 30,
                            titleFontColor: "#C24642",
                           suffix: "kg"
                        }],
                        axisY2:[{
                            title: "",
                            lineColor: "#000",
                
                            labelFontColor: "BLUE",
                            titleFontColor: "#C24642",
                           suffix: "kg"
                        }],
                        
                        axisX:[{
                            title: "MESES",
                            lineColor: "#000",
                            interval: 1,
                
                            labelFontColor: "BLUE",
                            titleFontColor: "#C24642",
                         
                        }],

               
        
                        data: [{
                            type: "line",
                            name: "MAX PESO",
                            color: "BLUE",
                            markerSize: 0,
                            markerType: "none",
                            showInLegend: true,
                            dataPoints: [
                                @foreach($peso_max as $key=>$value)
                                  <?php  echo   "{ x:".$key.", y:". $value ." },";?> 
                                @endforeach

                            ]
                        },
                        {
                            type: "line",
                            name: "MIN PESO",
                            color: "RED",
                            showInLegend: true,
                            markerSize: 0,
                            markerType: "none",
                            dataPoints: [
                                @foreach($peso_min as $key=>$value)
                                  <?php  echo   "{ x:".$key.", y:". $value ." },";?> 
                                @endforeach
                            ]
                        },
                        {
                            type: "line",
                            name: "PESO",
                            color: "GREEN",
                            showInLegend: true,
                            dataPoints: [
                                @foreach($pesos as $ps)
                                  <?php  if($ps->peso>0){
                                      echo   "{ x:".$ps->mes.", y:". $ps->peso ." },";
                                  }
                                
                                  ?> 
                                @endforeach
                            
                            ]
                        }],
                           pointRadius: 0
                    });
                    chart.render();

                    function toggleDataSeries(e) {
                        if (typeof (e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
                            e.dataSeries.visible = false;
                        } else {
                            e.dataSeries.visible = true;
                        }
                        e.chart.render();
                    }

                }
            </script>
       @endif
          <!-- /.box -->
        </div>
        <div class=col-md-6>
            <div class="box box-primary">
                <div class="box-header with-border">
                <h3 class="box-title">EVENTOS</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                <table id="eventos" class="table table-bordered ">
                        <thead>
                        <tr>
                            <th>Fecha</th>
                            <th>Evento</th>
                            <th>Opciones</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($eventos as $e)
                            <tr>
                            <td>
                            {{$e->fecha}}
                            </td>
                            <td>
                            @switch($e->tipo)
                                @case(1)
                                    <span>PESO</span>
                                    @break

                                    @case(2)
                                    <span>HERRERO</span>
                                    @break

                                    @case(3)
                                    <span>HISTORIA REPRODUCTIVA</span>
                                    @break

                                    @case(4)
                                    <span>SANIDAD</span>
                                    @break
                                    @case(5)
                                    <span>PROGRAMA ANTIPARASITARIO</span>
                                    @break
                                    @case(6)
                                       <span>FOTO</span>
                                    @break
                                    @case(7)
                                        <span>VACUNAS</span>
                                    @break
                                @default
                                    <span>Something went wrong, please try again</span>
                            @endswitch
                            </td>
                            <td>
                            <a  href="/caballos/evento/detalle/{{$e->id}}" 
                            class="btn btn-info" title="Detalles">Ver</a>
                            @if($e->tipo ==3)
                                <a  href="/caballos/evento/editar/{{$e->id}}" 

                                class="btn btn-info" title="Borrar">Editar</a>
                            @endif
                            @if($e->tipo !=5)
                                <a  href="/caballos/evento/eliminar/{{$e->id}}" 
                                onclick="return confirm('Desea eliminar el evento ?')"
                                class="btn btn-danger" title="Borrar">Eliminar</a>
                            @endif
                            </td>
                
                            </tr>
                        @endforeach

                        </tbody>
                    </table>
            
            </div>
        </div>
    </div>
</div>



@stop

@section("js")
<script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>
  <script>
  
    document.getElementById('upload').addEventListener('click', openDialog);

    function openDialog() {
      document.getElementById('file').click();
    }
    $('#foto-perfil input').change(function() {
         $(this).closest('form').submit();
    });

    $('#pedigree input').change(function() {
         $(this).closest('form').submit();
    });
     function thisFileUpload() {
        document.getElementById('file_pedigree').click();
        };
    $('#eventos').DataTable( {
        "order": [[0, "desc" ]],
                "language":{
                    "sProcessing":     "Procesando...",
                    "sLengthMenu":     "Mostrar _MENU_ registros",
                    "sZeroRecords":    "No se encontraron resultados",
                    "sEmptyTable":     "Ningún dato disponible en esta tabla",
                    "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                    "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
                    "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                    "sInfoPostFix":    "",
                    "sSearch":         "Buscar:",
                    "sUrl":            "",
                    "sInfoThousands":  ",",
                    "sLoadingRecords": "Cargando...",
                    "oPaginate": {
                        "sFirst":    "Primero",
                        "sLast":     "Último",
                        "sNext":     "Siguiente",
                        "sPrevious": "Anterior"
                    },
                    "oAria": {
                        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                    }
                }
            } );
</script>
@stop
<?php /**
 * Created by PhpStorm.
 * User: raffa
 * Date: 23/9/2018
 * Time: 2:35 PM
 */ ?>
@extends('adminlte::page')

@section('title', 'Eventos - Haras - Rapetti')

@section('content_header')
    <h1>Crear Evento <a class="btn btn-success" href="/caballos">< Lista de Caballos </a></h1>
@stop

@section('content')
<link href="{{asset('css/multi-select.css') }}" media="screen" rel="stylesheet" type="text/css">
<link href="{{asset('css/multi-select.dist.css') }}" media="screen" rel="stylesheet" type="text/css">

<div class="row">
    <div class="box">
          <div class="row" style="margin-left:10px;">
            
    
                <div class="col-md-12">
  <br>
                    <div class="event ">
                        <form  method="post" action="{{url('caballos/alimentacion')}}" enctype="multipart/form-data">         {!! csrf_field() !!}
                              <input type="hidden" name="tipo" value="5">
                              <div class="row">
                                    <div class="form-group col-xs-12 col-sm-8 col-md-5">
                                        <label for="caballo">Selecione Caballos</label> 
                                        <select  class="form-control required" required     multiple="multiple" id="caballos" name="caballos[]">
                                        @foreach($caballos as $c)
                                            <option style="max-width:100px;" value="{{$c->id}}">{{$c->nombre}}</option>        
                                        @endforeach
                                        </select>
                                    </div>
                                 </div>
                                 <div class="row">
                                    <div class="col-xs-10 col-sm-8 col-md-4 ">
                                            <label for="alimentacion">Alimentación </label> 
                                            <select  class="form-control required" id="alimentacion" style="max-width:372px;" name="alimentacion">
                                                    <option value="1">Yeguas madre</option>    

                                                    <option value="2">Potrillos desde 2 meses hasta 1 año</option>     

                                                    <option value="3">Potrillos de 1 a 2 años</option> 

                                                    <option value="4">Caballos en recuperación o Descanso</option> 
                                            </select>
                                            </div>
                                
                                    </div>
                               </div>
                           <br>
                         <button type="submit" class="btn btn-info"> Cambiar </button>
                        </form>

                    </div>
                  
                    
                </div>
         <br>
        </div>
    </div>
</div>



@stop

@section("js")
  <script>


    $( document ).ready(function() {
        $('#caballos').multiSelect({
                    selectableHeader: "<input type='text' class=' form-control search-input' autocomplete='off' placeholder='Buscar'>",
                    selectionHeader: "<input type='text' class=' form-control  search-input' autocomplete='off' placeholder='Buscar'>",
                    afterInit: function(ms){
                        var that = this,
                            $selectableSearch = that.$selectableUl.prev(),
                            $selectionSearch = that.$selectionUl.prev(),
                            selectableSearchString = '#'+that.$container.attr('id')+' .ms-elem-selectable:not(.ms-selected)',
                            selectionSearchString = '#'+that.$container.attr('id')+' .ms-elem-selection.ms-selected';

                        that.qs1 = $selectableSearch.quicksearch(selectableSearchString)
                        .on('keydown', function(e){
                        if (e.which === 40){
                            that.$selectableUl.focus();
                            return false;
                        }
                        });

                        that.qs2 = $selectionSearch.quicksearch(selectionSearchString)
                        .on('keydown', function(e){
                        if (e.which == 40){
                            that.$selectionUl.focus();
                            return false;
                        }
                        });
                    },
                    afterSelect: function(){
                        this.qs1.cache();
                        this.qs2.cache();
                    },
                    afterDeselect: function(){
                        this.qs1.cache();
                        this.qs2.cache();
                    }
                    });
  
                  
});
</script>
<script src="{{asset('js/jquery.multi-select.js')}}" type="text/javascript"></script>
<script>


</script>
<style>

form button{
  margin-bottom: 40px!important;
    margin-top: 17px;
}
}
</style>
@stop
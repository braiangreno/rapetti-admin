<?php /**
 * Created by PhpStorm.
 * User: raffa
 * Date: 23/9/2018
 * Time: 2:35 PM
 */ ?>
@extends('adminlte::page')

@section('title', 'Caballos - Haras - Rapetti')

@section('content_header')
    <h1>Eventos {{$caballo->nombre ?? ''}} <a href="/caballos/eventos" class="btn btn-block btn-success btn" style="    width: auto;    display: inline;">Agregar +</a>
    </h1>
@stop

@section('content')
   
  <div class="row">
  <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="box">
            <div class="box-header">
            Eventos
            </div>
            <div class="box-body table-responsive">
                <table id="eventos" class="table table-bordered ">
                    <thead>
                    <tr>
                        <th>Nro</th>
                        <th>Evento</th>
                        <th>Opciones</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($eventos as $e)
                        <tr>
                        <td>
                        {{$e->id}}
                        </td>
                        <td>
                        @switch($e->tipo)
                            @case(1)
                                <span>PESO</span>
                                @break

                                @case(2)
                                <span>HERRERO</span>
                                @break

                                @case(3)
                                <span>Historia Reproductiva</span>
                                @break

                                @case(4)
                                <span>SANIDAD</span>
                                @break
                                @case(5)
                                <span>PROGRAMA ANTIPARASITARIO</span>
                                @break
                                @case(6)
                                <span>FOTO</span>
                                @break

                                @case(7)
                                <span>VACUNAS</span>
                                @break
                            @endswitch                  
                        </td>
                        <td>
                        <a  href="/caballos/evento/detalle/{{$e->id}}" 
                         class="btn btn-info" title="Detalles">Ver</a>
                         @if($e->tipo ==3)
                                <a  href="/caballos/evento/editar/{{$e->id}}" 

                                class="btn btn-info" title="Borrar">Editar</a>
                            @endif
                         @if($e->tipo !=5)
                            <a  href="/caballos/evento/eliminar/{{$e->id}}" 
                            onclick="return confirm('Desea eliminar el evento ?')"
                            class="btn btn-danger" title="Borrar">Eliminar</a>
                        @endif
                        </td>
             
                        </tr>
                    @endforeach

                    </tbody>
                </table>
            </div>

        </div>
    </div>
    </div>


    <div id="modalDelete" class="modal modal-danger">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="titulo-modal-delete"></h4>
                </div>
                <form id="form_delete" method="post">
                    <div class="modal-footer">
                        <input type="hidden" id="modal_delete_id" name="modal_delete_id">
                        <button type="button " class="btn   btn-outline pull-left" data-dismiss="modal">CANCELAR</button>
                        <button type="submit" class="btn  btn-success">CONFIRMAR</button>

                    </div>
                </form>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->



@stop

@section("js")
    <script>
        $( document ).ready(function() {

            $('#eventos').DataTable( {
                "language":{
                    "sProcessing":     "Procesando...",
                    "sLengthMenu":     "Mostrar _MENU_ registros",
                    "sZeroRecords":    "No se encontraron resultados",
                    "sEmptyTable":     "Ningún dato disponible en esta tabla",
                    "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                    "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
                    "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                    "sInfoPostFix":    "",
                    "sSearch":         "Buscar:",
                    "sUrl":            "",
                    "sInfoThousands":  ",",
                    "sLoadingRecords": "Cargando...",
                    "oPaginate": {
                        "sFirst":    "Primero",
                        "sLast":     "Último",
                        "sNext":     "Siguiente",
                        "sPrevious": "Anterior"
                    },
                    "oAria": {
                        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                    }
                }
            } );




            ///////////////////





        });
    </script>

@stop
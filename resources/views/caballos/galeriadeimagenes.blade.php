<?php /**
 * Created by PhpStorm.
 * User: raffa
 * Date: 23/9/2018
 * Time: 2:35 PM
 */ ?>
@extends('adminlte::page')

@section('title', 'Caballos - Haras - Rapetti')

@section('content_header')
    <h1>Galeria de Imagenes  <a class="btn btn-success" href="/caballos">< Lista de Caballos </a></h1>
@stop

@section('content')
<div class="row ">
    <div class="col-xs-12 col-md-6 col-sm-8" >

        <form  method="post" action="/caballos/cargarimagen" enctype="multipart/form-data" style="display:inline-flex;margin-bottom:20px;">
        <button class="btn btn-info">CARGAR</button> 
        {{ csrf_field() }}
        <input class="form-control required" type="file" id="file" name="file" />  
        <input type="hidden" name="id" value="{{$id}}">
    </form>
      
        
    </div>

   <div class="col-xs-12 col-sm-12 col-md-12">
       
            <div class="row" style="position:relative">
            @foreach($fotos as $f)
                <div class="col-xs-6 col-md-3" style="min-height:250px;
    background-color: grey;
    background-image:url('../..{{$f->url}}');
    background-position: center;
    background-size: contain;
    background-repeat: no-repeat;">
        
                        <a href="eliminar/{{$f->id}}" style="position: absolute;
                                                            top: -11px;
                                                            left: 10px;
                                                            font-size: 39px;
                                                            transform: scaleX(1.5);
                                                            color: red;
                                                        }">X</a>
                </div>
                
            @endforeach
     
    </div>
</div>



@stop

@section("js")
  <script>
  
  </script>
@stop
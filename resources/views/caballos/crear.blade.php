<?php /**
 * Created by PhpStorm.
 * User: raffa
 * Date: 23/9/2018
 * Time: 2:35 PM
 */ ?>
@extends('adminlte::page')

@section('title', 'Caballos - Haras - Rapetti')

@section('content_header')
    <h1>Crear Caballo </h1>
@stop

@section('content')
<div class="row">
<link href="{{asset('css/multi-select.css') }}" media="screen" rel="stylesheet" type="text/css">
<link href="{{asset('css/multi-select.dist.css') }}" media="screen" rel="stylesheet" type="text/css">
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="box">
        <form id="form_crear_caballo"  method="post" action="{{url('caballos/create')}}" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="box-body">
                        <div class="row">
                            <div class="col-xs-12 col-sm-8 col-md-5 ">
                                        <label for="Nombre">Nombre</label>
                                        <input type="text" class="form-control required"  required name="Nombre" id="Nombre" placeholder="Nombre">
                            </div>
                        </div>
                        
                        <div class="row">
                        <div class="col-xs-12 col-sm-8 col-md-5 ">
                                        <label for="Sexo">Sexo</label>
                                        <select class="form-control required" name="Sexo">
                                            <option value="macho"> Macho</option>
                                            <option value="Hembra"> Hembra</option>
                                        </select>
                            </div>
                        </div>
                        <div class="row">
                        <div class="col-xs-12 col-sm-8 col-md-5 ">
                                        <label for="Pelo">Pelo</label>
                                        <input type="text" class="form-control required"  required name="Pelo" id="Pelo" placeholder="Pelo">
                       
                            </div>
                        </div>
                        <div class="row">
                        <div class="col-xs-12 col-sm-8 col-md-5 ">
                                        <label for="Padre">Padre</label>
                                        <input type="text" class="form-control required"  required name="Padre" id="Padre" placeholder="Padre">
                       
                            </div>
                        </div>
                        <div class="row">
                        <div class="col-xs-12 col-sm-8 col-md-5 ">
                                        <label for="Madre">Madre</label>
                                        <input type="text" class="form-control required"  required name="Madre" id="Madre" placeholder="Madre">
                       
                            </div>
                        </div>
                        <div class="row">
                        <div class="col-xs-12 col-sm-8 col-md-5 ">
                                        <label for="Peso">Peso</label>
                                        <input type="number" step="0.01" min="1" max="10000" class="form-control required"  required name="Peso" id="Peso" placeholder="Peso">
                       
                            </div>
                        </div>
                     
                        <div class="row">
                        <div class="col-xs-12 col-sm-8 col-md-5 ">
                                        <label for="fechanacimiento">Fecha de Nacimiento</label>
                                        <input type="date" class="form-control required"  required name="fechanacimiento" id="fechanacimiento" placeholder="fechanacimiento">
                       
                            </div>
                        </div>
                        <div class="row">
                        <div class="col-xs-12 col-sm-8 col-md-5 ">
                                        <label for="fechaingreso">Fecha de ingreso</label>
                                        <input type="date" class="form-control required"  required name="fechaingreso" id="fechaingreso" placeholder="fechaingreso">
                       
                            </div>
                        </div>
                        <div class="row">
                        <br>
                            <div class="col-xs-12 col-sm-8 col-md-5 ">
                                        <label for="propietarios[]">Propietarios </label>
                                        <select class="form-control required" required multiple="multiple" id="propietarios" name="propietarios[]">
                                            @foreach($propietarios as $p)
                                            <option style="max-width:100px" value='{{$p->id}}'>{{$p->name ." ". $p->lastname}} </option>
                                            @endforeach
                                        
                                        </select>
                       
                            </div>
                        </div>
    
                        <div class="row">
                                <div class="col-xs-12 col-sm-8 col-md-5 ">
                                <br>
                                        <label for="e2_file">Foto</label>
                                
                                        <input class="form-control required " required
                                        type="file" id="e2_file" name="e2_file" />
                    
                                </div>
                            </div>  
                        <div class="row">
                        
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-sm-8 col-md-5 ">
                                        <label for="Observaciones2">Observaciones</label>
                                        <textarea class="form-control"   name="Observaciones2" id="Observaciones2" placeholder="Observaciones"></textarea>
                       
                            </div>
                        </div>
                        <div class="row">
                                    <div class="col-xs-12 col-sm-8 col-md-4 ">
                                            <label for="alimentacion">Alimentación </label> 
                                            <select  class="form-control required" id="alimentacion" name="alimentacion">
                                                    <option value="1">Yeguas madre</option>    

                                                    <option value="2">Potrillos desde 2 meses hasta 1 año</option>     

                                                    <option value="3">Potrillos de 1 a 2 años</option> 

                                                    <option value="4">Caballos en recuperación o Descanso</option> 
                                            </select>
                                            </div>
                                
                                    </div>
                               </div>
                                            

                     
                    <button type="submit" class="btn btn-success" style="padding:15px;font-size:20px;margin-top:20px;">ENVIAR</button>
                    </div><!-- /.box-body -->
                </form>

        </div>
    </div>
</div>



@stop

@section("js")
<script src="{{asset('js/jquery.multi-select.js')}}" type="text/javascript"></script>
    <script>
        $('#propietarios').multiSelect({
  selectableHeader: "<div class='custom-header'>Disponibles</div>",
  selectionHeader: "<div class='custom-header'>Propietarios</div>",
  selectableFooter: "<div class='custom-header'>Disponibles</div>",
  selectionFooter: "<div class='custom-header'>Asignados</div>"
})

$(document).ready(function (e) {
	
    	$('#upload').on('click', function () {
  
            if($('#file').val() == ''){
                alert("selecione una imagen");
            }else{
            var file_data = $('#file').prop('files')[0];
            var form_data = new FormData();
			form_data.append('file', file_data);
			$.ajax({
                headers: {'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')},
				url: '/imagenes/upload', // point to server-side PHP script 
				dataType: 'text', // what to expect back from the PHP script
				cache: false,
				contentType: false,
				processData: false,
				data: form_data,
				type: 'post',
				success: function (resp) {
                    console.log(resp);
                    datos= JSON.parse(resp);
                    $('#message').css('display', 'block');
                    $('#message').html(datos.message);
                    $('#message').addClass(datos.class_name);
                    $("#inputhidden-file").val( $("#inputhidden-file").val()+","+datos.id);
                    $('#uploaded_image').append(
                        '<div style="position:relative" id="'+datos.image_name +'" > <img src="/imagenes_subidas/'+ datos.uploaded_image +'" class="img-thumbnail img-responsive" style="height:120px!important;"><div class="close-image" data-id="'+datos.image_name +'" style=    "font-size: 40px;    position: absolute;  top: -8px;    left: 11px;    color:red;    height: 20px;    transform: scaleX(1.3);"><span>X</span></div> </div>'
                    );
                   
				},
				error: function (resp) {
				//	$('#msg').html(response); // display error response from the PHP script
				}
            });
            }
		
            
        });
        
      
    });
                      $(".close-image").live("click", function(){
                          
                                $("#".  $(this).data('id')).hide();  
                    });

    </script>
<style>
.close-image:hover{cursor:pointer;
}</style>
@stop
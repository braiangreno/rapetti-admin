<?php /**
 * Created by PhpStorm.
 * User: raffa
 * Date: 23/9/2018
 * Time: 2:35 PM
 */ ?>
@extends('adminlte::page')

@section('title', 'Peso - Haras - Rapetti')

@section('content_header')
    <h1>Modificar Peso Potrillo <a class="btn btn-success" href="/caballos">< Lista de Caballos </a></h1>
@stop

@section('content')
<link href="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css" rel="stylesheet" />
<div class="row">
    <div class="box">
          <div class="row" style="margin-left:10px;">
            
    
                <div class="col-md-12">
  <br>
                    <div class="event ">
                        <form  method="post" action="{{url('caballos/pesos')}}" enctype="multipart/form-data">         {!! csrf_field() !!}
                              <div class="row">
                                    <div class="form-group col-xs-12 col-sm-8 col-md-5">
                                        <label for="caballo">Selecione Caballos</label> 
                                        <select  class="form-control required" required   id="caballo" name="caballo">
                                        <option style="max-width:100px;" value="0"></option>    
                                        @foreach($caballos as $c)
                                            <option style="max-width:100px;" value="{{$c->id}}">{{$c->nombre}}</option>        
                                        @endforeach
                                        </select>
                                    </div>
                                 </div>
                            <div class="row">
                            <div  id="contenido-pesos" class="form-group col-xs-12 col-sm-8 col-md-5">
                            </div>
                                </div>
                           <br>
                         <button type="submit" id="actualizar" class="btn btn-info"> Actualizar </button>
                        </form>

                    </div>
                  
                    
                </div>
         <br>
        </div>
    </div>
</div>



@stop

@section("js")

<script src="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js"></script>

  <script>

    $("#actualizar").hide();
    $( document ).ready(function() {
        $('#caballo').select2({
    matcher: matchCustom
});
  
                  
});
$('#caballo').on('change', function () {


  $.ajax({
      headers: {'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')},
      url: '/caballos/pesosget/'+$("#caballo").val(), // point to server-side PHP script 
      dataType: 'html', // what to expect back from the PHP script
      cache: false,
      contentType: false,
      processData: false,
      data: {
                    },
      type: 'post',
      success: function (resp) {
          
        $("#contenido-pesos").html(resp);
        $("#actualizar").show();
      },
      error: function (resp) {
      //	$('#msg').html(response); // display error response from the PHP script
      }
  });


  
});
function matchCustom(params, data) {
            // If there are no search terms, return all of the data
            if ($.trim(params.term) === '') {
            return data;
            }

            // Do not display the item if there is no 'text' property
            if (typeof data.text === 'undefined') {
            return null;
            }

            // `params.term` should be the term that is used for searching
            // `data.text` is the text that is displayed for the data object
            if (data.text.indexOf(params.term) > -1) {
            var modifiedData = $.extend({}, data, true);
            modifiedData.text += ' (matched)';

            // You can return modified objects from here
            // This includes matching the `children` how you want in nested data sets
            return modifiedData;
            }

            // Return `null` if the term should not be displayed
            return null;
        }
</script>
<script src="{{asset('js/jquery.multi-select.js')}}" type="text/javascript"></script>
<script>


</script>
<style>

form button{
  margin-bottom: 40px!important;
    margin-top: 17px;
}

</style>
@stop
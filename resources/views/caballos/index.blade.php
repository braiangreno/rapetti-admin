<?php /**
 * Created by PhpStorm.
 * User: raffa
 * Date: 23/9/2018
 * Time: 2:35 PM
 */ ?>
@extends('adminlte::page')

@section('title', 'Caballos - Haras - Rapetti')

@section('content_header')
    <h1>Caballos <a href="/caballos/crear" class="btn btn-block btn-success btn" style="    width: auto;    display: inline;    float: right;">Agregar +</a>
    </h1>
@stop

@section('content')
@if(Session::has('message')) 
                <p class="alert alert-success">{{ Session::get('message') }}</p>
              @endif
<link href="{{asset('css/multi-select.css') }}" media="screen" rel="stylesheet" type="text/css">
<link href="{{asset('css/multi-select.dist.css') }}" media="screen" rel="stylesheet" type="text/css">
  <div class="row">
                   <div class="col-md-12">
                        <div class="row">
                                <div class="form-group col-xs-12 col-sm-8 col-md-5">
                                    <label for="search_prop">Busqueda por propietarios</label> 
                                    <select class="form-control" name="search_prop" id="search_prop">
                                    <option value="">Seleccione</option>
                                    @if(isset($user_id))
                                    @foreach($propietarios as $propietario)
                                        <option value="{{$propietario->id}}" '@if($user_id == $propietario->id) selected @endif'>
                                        {{$propietario->name}} {{$propietario->lastname}}
                                        </option>
                                    @endforeach
                                    @else
                                        @foreach($propietarios as $propietario)
                                        <option value="{{$propietario->id}}">{{$propietario->name}} {{$propietario->lastname}}</option>
                                        @endforeach
                                    @endif    
                                    </select>
                                </div>
                        </div>
                 </div>
  <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="box">
            <div class="box-header">
            Caballos
            </div>
            <div class="box-body table-responsive">
                <table id="caballos" class="table table-bordered  table-responsive ">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Perfil</th>
                        <th>Nombre</th>
                        <th>Sexo</th>
                        <th>Peso</th>
                        <th>Opciones</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($caballos as $c)
                        <tr>
                        <td>{{$c->id}}</td>
                        <td style="position:relative; margin:0 auto;text-align:center;">
                                <a href="/caballos/perfil/adm/{{$c->id}}">
                                @if($c->foto_perfil_id==0)
                                    <img class="profile-user-img  img-circle" style="width:100px;height:100px" src="/imagenes_subidas/empty.png" alt="User profile picture">
                                @else
                                    <img class="profile-user-img img-circle" style="width:100px;height:100px" src="{{$c->foto_perfil_url}}" alt="User profile picture">
                    
                                @endif

                                </a>
                        </td>
                        <td>{{$c->nombre}}</td>
                        <td>{{$c->sexo}}</td>
                        <td>{{$c->peso}}</td>
                        <td>
                        <a href="#"
                                   data-id="{{$c->id}}"
                                   data-nombre="{{$c->nombre}}"
                                   data-madre="{{$c->madre}}"
                                   data-padre="{{$c->padre}}"
                                   data-sexo="{{$c->sexo}}"
                                   data-pelo="{{$c->pelo}}"
                                   data-observaciones="{{$c->observaciones}}"
                                   data-fechaingreso="{{$c->fecha_de_ingreso}}"
                                   data-fechanacimiento="{{$c->fecha_de_nacimiento}}"
                                   data-toggle="modal"
                                   data-target="#modalEdit"
                                   data-skin="skin-blue" title="Editar" class="btn btn-primary modal-opening-edit"><i class="fa fa-edit"></i></a>
                      
                      
                      
                        <a href="caballos/imagenes/{{$c->id}}" class="btn btn-info" title="galeria de imagenes">Imagenes</a>
                        <a href="caballos/videos/{{$c->id}}"  class="btn btn-primary" title="galeria de videos">Videos</a>
                        <a  href="caballos/eventos/{{$c->id}}" class="btn btn-warning" title="eventos">Eventos</a>
                        <div id="btn_{{$c->id}}" data-id="{{$c->id}}" data-type="{{$c->state}}" class="form_activar btn @if($c->state==1) btn-danger @else btn-success @endif"
                         title="activar">@if($c->state==1)Desactivar @else Activar @endif</div>
                        <a  href="caballos/eliminar/{{$c->id}}" 
                        onclick="return confirm('Desea eliminar el caballo {{$c->nombre}}?')"
                         class="btn btn-danger" title="Borrar">Eliminar</a>
                       
             
                        <form  id="pedigree{{$c->id}}" method="post" action="/caballos_pedigree_up2/{{$c->id}}" enctype="multipart/form-data">
                                    {{ csrf_field() }}
                                    <input class="form-control" type="file" id="file_pedigree{{$c->id}}" name="file_pedigree{{$c->id}}" style="opacity:0;height:0;"/>
                                    <input type="hidden" name="id" value="{{$c->id}}">
                                
                                    <input type="button" id="upload_pedigree{{$c->id}}" hidden class="btn hidden btn-primary btn-block">
                                </form>
                                <button  class="btn btn-info" onclick="thisFileUpload{{$c->id}}();" title="Cargar pedigree"
                                    > Cargar Pedigree</button> 

  

                                    </td>
                        </tr>
                    @endforeach

                    </tbody>
                </table>
            </div>

        </div>
    </div>
    </div>


    <div id="modalDelete" class="modal modal-danger">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="titulo-modal-delete"></h4>
                </div>
                <form id="form_delete" method="post">
                    <div class="modal-footer">
                        <input type="hidden" id="modal_delete_id" name="modal_delete_id">
                        <button type="button " class="btn   btn-outline pull-left" data-dismiss="modal">CANCELAR</button>
                        <button type="submit" class="btn  btn-success">CONFIRMAR</button>

                    </div>
                </form>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    <div id="modalEdit" class="modal modal-primary">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="titulo-modal-edit"></h4>
                </div>
                <form id="form_edit" method="post">
                <div class="modal-body">

                    <input  type="hidden" name="modal_edit_id" id="modal_edit_id">
                    <table class="table table-condensed">
                        <tr>
                            <td><strong>Nombre</strong></td>
                            <td>
                                <input type="text" class="form-control required"  required name="modal_edit_nombre" id="modal_edit_Nombre" >
                            </td>
                        </tr>
                        <tr>
                            <td><strong>Sexo</strong></td>
                            <td>
                            <select class="form-control required" name="modal_edit_Sexo" id="modal_edit_Sexo">
                                            <option value="Macho"> Macho</option>
                                            <option value="Hembra"> Hembra</option>
                                        </select>
                            </td>
                        </tr>
                        <tr>
                            <td><strong>Pelo</strong></td>
                            <td>
                                <input type="text" class="form-control required"  required name="modal_edit_Pelo" id="modal_edit_Pelo" >
                            </td>
                        </tr>
                      
                        <tr>
                            <td><strong>Madre</strong></td>
                            <td >
                                <input type="text" class="form-control required"  required name="modal_edit_Madre" id="modal_edit_Madre" >

                            </td>
                        </tr>
                        <tr>
                            <td><strong>Padre</strong></td>
                            <td>
                                <input type="text" class="form-control required"  required name="modal_edit_Padre" id="modal_edit_Padre">
                            </td>
                        </tr>
                        <tr>
                            <td><strong>Fecha de Ingreso</strong></td>
                            <td>
                            <input type="date" class="form-control required"  required name="modal_edit_fechaingreso" id="modal_edit_fechaingreso" placeholder="fechaingreso">
                            </td>
                        </tr>
                        <tr>
                            <td><strong>Fecha de Nacimiento</strong></td>
                            <td>
                 

                            <input type="date" class="form-control required"  required name="modal_edit_fechanacimiento" id="modal_edit_fechanacimiento" placeholder="fechanacimiento">
                       
                       
                            
                            </td>
                        </tr> 
                        <tr>
                            <td><strong>Observaciones</strong></td>
                            <td>
                                <textarea  class="form-control"   name="modal_edit_Observaciones" id="modal_edit_Observaciones"></textarea>
                            </td>
                        </tr>

                        <tr>
                        <td colspan="2">
                        <label for="modal_edit_propietarios[]">Propietarios </label>
                                        <select class="form-control required" required multiple="multiple" id="modal_edit_propietarios" name="modal_edit_propietarios[]">         
                        </select>
                        </td>
                        </tr>
                        <tr>
                            <div class="form-group has-error">
                                <label class="control-label" for="inputError"><i class="fa fa-times-circle-o" id="mensaje-error-edit"></i>
                                </label>

                            </div>
                        </tr>


                    </table>
                </div>
                <div class="modal-footer">
                    <button type="button " class="btn   btn-outline pull-left" data-dismiss="modal">Cerrar</button>
                    <button type="submit" class="btn btn-outline">Actualizar<i class="fa fa-refresh spin" style="display:none" id="cargando_form_edit"> </i></button>

                </div>
                </form>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->



@stop

@section("js")
    <script src="{{asset('js/jquery.multi-select.js')}}" type="text/javascript"></script>
    <script>
        $('#modal_edit_propietarios').multiSelect({
  selectableHeader: "<div class='custom-header'>Disponibles</div>",
  selectionHeader: "<div class='custom-header'>Propietarios</div>",
  selectableFooter: "<div class='custom-header'>Disponibles</div>",
  selectionFooter: "<div class='custom-header'>Asignados</div>"
});

@foreach($caballos as $c)

    $('#pedigree{{$c->id}} input').change(function() {
            $(this).closest('form').submit();
        });
     function thisFileUpload{{$c->id}}() {
        document.getElementById('file_pedigree{{$c->id}}').click();
        };
@endforeach
        $("#crear-nuevo-cliente").hide();
        $("#mensaje-error").hide();
        $("#mensaje-error-edit").hide();



        $('#modalDelete').on('hide.bs.modal', function (e) {
            $("#modal_delete_id").val('');

        });




        $('.modal-opening-delete').on('click', function (e) {
            var modal_id = $(this).data('id');
            var modal_nombre = $(this).data('nombre');
            console.log(modal_nombre);

            $("#titulo-modal-delete").html("¿ Seguro desea eliminar el cliente #"+modal_id+" : "+modal_nombre +"?");

            $("#modal_delete_id").val(modal_id);

        })
        $( document ).ready(function() {

            $('#caballos').DataTable( {
               // "order": [[ 2, "asc" ]],
               "ordering": false,
                "pageLength": 100,
                "language":{
                    "sProcessing":     "Procesando...",
                    "sLengthMenu":     "Mostrar _MENU_ registros",
                    "sZeroRecords":    "No se encontraron resultados",
                    "sEmptyTable":     "Ningún dato disponible en esta tabla",
                    "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                    "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
                    "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                    "sInfoPostFix":    "",
                    "sSearch":         "Buscar:",
                    "sUrl":            "",
                    "sInfoThousands":  ",",
                    "sLoadingRecords": "Cargando...",
                    "oPaginate": {
                        "sFirst":    "Primero",
                        "sLast":     "Último",
                        "sNext":     "Siguiente",
                        "sPrevious": "Anterior"
                    },
                    "oAria": {
                        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                    }
                }

            } );
            var clickeado=0;
            var token = $('#_token').val();
            $('#agregar_cliente').click(function(e) {
                if(clickeado==0){
                    $("#crear-nuevo-cliente").slideToggle()
                    clickeado++;
                }

            });

            $('#cerrar-nuevo-cliente').click(function(e) {
                if(clickeado>0){
                    $("#crear-nuevo-cliente").slideToggle()
                    clickeado=0;
                }

            });

            $('.form_activar').click(function(e) {
              
                var route = "/caballos/activar";
                    name="#btn_"+$(this).data('id');
                $.ajax({
                    
                    url: route,
                    headers: {'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')},
                    type: 'GET',
                    data: {
                        'id':$(this).data('id'),
                        'type':$(this).data('type'),
                    },
                    success: function(result)
                    {
                       
                        if(result != 'null' && result.msg!='fail'){
                            
                            if($(name).data('type')==0){
                                $(name).data('type',1);
                                $(name).removeClass('btn-success');
                                $(name).addClass('btn-danger');
                                $(name).text('Desactivar');
                            }else{
                                $(name).data('type',0);
                                $(name).addClass('btn-success');
                                $(name).removeClass('btn-danger');
                                $(name).text('Activar');
                            }
            
                        }

                    }

                });
            });

            ///////////////////
            $('#form_delete').submit(function(ev) {
                ev.preventDefault();

                $("#cargando_form_edit").show();
                var Id=$("#modal_delete_id").val();
                console.log(Id);
                var route = "users/eliminar/"+Id+"/";
                $.ajax({
                    url: route,
                    headers: {'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')},
                    type: 'POST',
                    data: {
                        'id':Id
                    },
                    success: function(result)
                    {

                        if(result != 'null' && result.msg!='fail'){

                            setTimeout(
                                    function()
                                    {
                                        $("#modal_delete_id").val('');
                                        $('#modalDelete').modal('hide');
                                      location.reload();
                                    }, 1200);
                        }

                    }

                });
                return false;
            });






        });

                     ///////////////////
            $('#modalEdit').on('hide.bs.modal', function (e) {
                $("#modal_edit_Nombre").val('');
                  

                $("#modal_edit_Pelo").val('');
                $("#modal_edit_Madre").val('');
                $("#modal_edit_Padre").val('');
                $("#modal_edit_fechaingreso").val('');
                $("#modal_edit_fechanacimiento").val('');
                $("#modal_edit_propietarios").val('');
                $("#modal_edit_propietarios").empty();
                $("#modal_edit_Observaciones").text();
            });

        $('.modal-opening-edit').on('click', function (e) {
            var modal_id = $(this).data('id');
            var modal_nombre = $(this).data('nombre');
            var modal_madre = $(this).data('madre');
            var modal_padre = $(this).data('padre');
            var modal_sexo = $(this).data('sexo');
            var modal_pelo = $(this).data('pelo');
            var modal_fechaingreso = $(this).data('fechaingreso');
            var modal_fechanacimiento = $(this).data('fechanacimiento');
            var modal_propietarios =null;
            var modal_observaciones =$(this).data("observaciones");
      
              
              var route = "caballos/propietarios/"+modal_id;
                  name="#btn_"+$(this).data('id');
              $.ajax({
                  
                  url: route,
                  headers: {'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')},
                  type: 'GET',
                
                  success: function(result)
                  {
                    var datos = result.split("+");
                     var propietarios =  datos[0].split("|");
                     var usuarios =  datos[1].split("|");
                     if(propietarios!=""){
                        propietarios.forEach( function(valor, indice, array) {
                        var vals= valor.split(",");
                        $("#modal_edit_propietarios").append('<option value="'+vals[0]+'" selected >'+vals[1]+'</option>');

                        });    
                     }
                     if(usuarios!=""){
                        usuarios.forEach( function(valor, indice, array) {
                        vals= valor.split(",");
                        $("#modal_edit_propietarios").append('<option value="'+vals[0]+'"  >'+vals[1]+'</option>');

                        });               
                     }
                       
                       
                        $("#modal_edit_propietarios").multiSelect('refresh');
                }
              });
      
            $("#titulo-modal-edit").html("Caballo ID# "+modal_id);
            $("#modal_edit_Nombre").val(modal_nombre);
            $("#modal_edit_Madre").val(modal_madre);
            $("#modal_edit_Padre").val(modal_padre);
            $('#modal_edit_Sexo option[value='+modal_sexo+']').attr('selected','selected');
            $('#modal_edit_Sexo option[value='+modal_sexo+']').attr('selected','selected');
            $("#modal_edit_Pelo").val(modal_pelo);
            $("#modal_edit_Observaciones").val(modal_observaciones);
            
            $("#modal_edit_fechaingreso").val(modal_fechaingreso);
            $("#modal_edit_fechanacimiento").val(modal_fechanacimiento);
            $("#modal_edit_id").val(modal_id);

        })
            $('#form_edit').submit(function(ev) {
                ev.preventDefault();
                var route = "/caballos/update";
                $("#cargando_form_edit").show();
                var Id=$("#modal_edit_id").val();
                var Nombre = $("#modal_edit_Nombre").val();
                var Madre = $("#modal_edit_Madre").val();
                var Padre = $("#modal_edit_Padre").val();
                var Sexo = $( "#modal_edit_Sexo option:selected" ).text();
                var Pelo = $("#modal_edit_Pelo").val();
                var fechaingreso = $("#modal_edit_fechaingreso").val();
                var fechanacimiento = $("#modal_edit_fechanacimiento").val();
                var propietarios = $("#modal_edit_propietarios").val();
                var observaciones = $("#modal_edit_Observaciones").val();
                $("#modal_edit_Nombre").prop('disabled', true);
                $("#modal_edit_Madre").prop('disabled', true);
                $("#modal_edit_Padre").prop('disabled', true);
                $("#modal_edit_Sexo").prop('disabled', true);
                $("#modal_edit_Peo").prop('disabled', true);
                $("#modal_edit_fechaingreso").prop('disabled', true);
                $("#modal_edit_fechanacimiento").prop('disabled', true);
                $("#modal_edit_propietarios").prop('disabled', true);
                $("#modal_edit_Observaciones").prop('disabled', true);
                $.ajax({
                    url: route,
                    headers: {'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')},
                    type: 'POST',
                    data: {
                        'id':Id,
                        'nombre': Nombre,
                        'sexo': Sexo,
                        'madre': Madre,
                        'padre': Padre,
                        'pelo':Pelo,
                        'fecha_de_ingreso': fechaingreso,
                        'fecha_de_nacimiento': fechanacimiento,
                        'propietarios':propietarios,
                        'observaciones':observaciones
                    },
                    success: function(result)
                    {
                        
                        if(result != 'null' && result.msg!='fail'){

                            setTimeout(
                                    function()
                                    {
                                        $("#modal_edit_Nombre").val('');
                                        $("#modal_edit_Sexo").val('');
                                        $("#modal_edit_Pelo").val('');
                                        $("#modal_edit_Madre").val('');
                                        $("#modal_edit_Padre").val('');
                                        $("#modal_edit_fechaingreso").val('');
                                        $("#modal_edit_fechanacimiento").val('');
                                        $("#modal_edit_Nombre").prop('disabled', false);
                                        $("#modal_edit_Madre").prop('disabled', false);
                                        $("#modal_edit_Padre").prop('disabled', false);
                                        $("#modal_edit_Sexo").prop('disabled', false);
                                        $("#modal_edit_Pelo").prop('disabled', false);
                                        $("#modal_edit_Observaciones").prop('disabled', false);
                                        $("#modal_edit_fechaingreso").prop('disabled', false);
                                        $("#modal_edit_fechanacimiento").prop('disabled', false);
                                        $("#modal_edit_propietarios").empty();
                                            $("#cargando_form_edit").hide();
                                            $("#mensaje-error").hide();
                                            $('#modalEdit').modal('hide');
                                           location.reload();


                                    }, 1200);
                        }else{
                            $("#cargando_form_edit").hide();
                            $("#mensaje-error-edit").html(" "+result.data);
                            $("#mensaje-error-edit").show();
                            $("#modal_edit_Nombre").prop('disabled', false);
                            $("#modal_edit_Madre").prop('disabled', false);
                            $("#modal_edit_Padre").prop('disabled', false);
                            $("#modal_edit_Sexo").prop('disabled', false);
                            $("#modal_edit_Pelo").prop('disabled', false);
                            $("#modal_edit_propietarios").prop('disabled', false);
                            $("#modal_edit_fechaingreso").prop('disabled', false);
                            $("#modal_edit_fechanacimiento").prop('disabled', false);
                            $("#modal_edit_Observaciones").prop('disabled', false);

                        }

                    }

                });
                return false;
            });

 
            $( "#search_prop" ).change(function() {
                window.location.href = "{{ url('/caballos').'?search_prop=' }}"+$(this).val();
            });
      
    </script>

@stop
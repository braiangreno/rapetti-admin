<input hidden name="state" value={{$state}}>
<ul style="
    list-style-type: none;
    columns: 2;
    -webkit-columns: 2;
    -moz-columns: 2;">

@foreach($pesos as $p)
<li>
<div class="form-group">
<label for="mes_{{$p->mes}}" class="min-width: 50px!important;">MES {{$p->mes}}</label>
<input   name="mes_{{$p->mes}}" type="number" step="0.01" id="mes_{{$p->mes}}" value="{{$p->peso}}">
</div>
</li>
@endforeach

</ul>
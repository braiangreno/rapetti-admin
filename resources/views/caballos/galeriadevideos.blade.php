<?php /**
 * Created by PhpStorm.
 * User: raffa
 * Date: 23/9/2018
 * Time: 2:35 PM
 */ ?>
@extends('adminlte::page')

@section('title', 'Caballos - Haras - Rapetti')

@section('content_header')
    <h1>Galeria de Videos    @if($permiso==0)
                                  <a class="btn btn-success" href="/caballos">< Lista de Caballos </a>
                            @else
                            <a class="btn btn-success" href="/">< Lista de Caballos </a>
                            @endif
    
    </h1>
@stop

@section('content')
<div class="row ">
    <div class="col-xs-12 col-md-6 col-sm-8" >
    @if($permiso==0)
        <form  method="post" action="/caballos/cargarvideo" enctype="multipart/form-data" style="display:inline-flex;margin-bottom:20px;">
        <button class="btn btn-info">CARGAR</button> 
        {{ csrf_field() }}
        <input class="form-control required" type="file" id="file" name="file" />  
        <input type="hidden" name="id" value="{{$id}}">
    </form>
      @endif
        
    </div>

   <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="box" style="display:inline-flex;">
            <div class="row">
            @foreach($videos as $f)
                <div class="col-xs-12 col-md-6" >
                    <video style="max-height:300px!important;width:100%;" controls>
                            <source src="{{$f->url}}" typpe="video">
                    </video>  
                    @if($permiso==0)
                        <a href="eliminar/{{$f->id}}" style="position: absolute;
                                                            top: -11px;
                                                            left: 10px;
                                                            font-size: 39px;
                                                            transform: scaleX(1.5);
                                                            color: red;
                                                        }">X</a>
                    @endif
                </div>
            
            @endforeach
        </div>
    </div>
</div>



@stop

@section("js")
  <script>
  
  </script>
@stop
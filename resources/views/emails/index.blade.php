<?php /**
 * Created by PhpStorm.
 * User: raffa
 * Date: 23/9/2018
 * Time: 2:35 PM
 */ ?>
@extends('adminlte::page')

@section('title', 'Propietarios - Haras - Rapetti')

@section('content_header')
    <h1>Emails contacto 
    </h1>
        @stop

@section('content')

    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="box">
            <div class="box-header">
                Propietarios
            </div>
            <div class="box-body table-responsive">
                <table id="usuarios" class="table table-bordered ">
                    <thead>
                    <tr>
                        <th>Nombre</th>
                        <th>Email</th>
                        <th>Mensaje</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($emails as $c)
                        <tr>
                            <td>{{$c->id}}</td>
                            <td>{{$c->nombre }}</td>
                            <td>{{$c->email}}</td>
                            <td>{{$c->mensaje}}</td>
                        </tr>
                    @endforeach

                    </tbody>
                </table>
            </div>

        </div>
    </div>




@stop

@section("js")
    <script>
  
        $( document ).ready(function() {

            $('#clientes').DataTable( {
                "order": [[ 0, "desc" ]],
                "language":{
                    "sProcessing":     "Procesando...",
                    "sLengthMenu":     "Mostrar _MENU_ registros",
                    "sZeroRecords":    "No se encontraron resultados",
                    "sEmptyTable":     "Ningún dato disponible en esta tabla",
                    "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                    "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
                    "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                    "sInfoPostFix":    "",
                    "sSearch":         "Buscar:",
                    "sUrl":            "",
                    "sInfoThousands":  ",",
                    "sLoadingRecords": "Cargando...",
                    "oPaginate": {
                        "sFirst":    "Primero",
                        "sLast":     "Último",
                        "sNext":     "Siguiente",
                        "sPrevious": "Anterior"
                    },
                    "oAria": {
                        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                    }
                }
            } );
       
        });
    </script>

@stop
<?php /**
 * Created by PhpStorm.
 * User: raffa
 * Date: 23/9/2018
 * Time: 2:35 PM
 */ ?>
@extends('adminlte::page')

@section('title', 'Eventos - Haras - Rapetti')

@section('content_header')
    <h1>Crear Evento <a class="btn btn-success" style="float:right;" href="/caballos"> Lista de Caballos </a></h1>
@stop

@section('content')
<link href="{{asset('css/multi-select.css') }}" media="screen" rel="stylesheet" type="text/css">
<link href="{{asset('css/multi-select.dist.css') }}" media="screen" rel="stylesheet" type="text/css">
<link href="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/css/select2.min.css" rel="stylesheet" />

<div class="row">
    <div class="box">
          <div class="row" style="margin-left:10px;margin-right: 20px;">
            
                 <div class="col-md-12">
                        <div class="row">
                                <div class="form-group col-xs-12 col-sm-8 col-md-5">
                                    <label for="tipo_evento">Tipo de Evento</label> 
                                    <select class="form-control" name="tipo_evento" id="tipo_evento">
                                        <option value="1">Evolucion de Peso</option>
                                        <option value="2">HERRERO</option>
                                        <option value="3">Historia Reproductiva</option>
                                        <option value="4">Sanidad</option>
                                        <option value="5">programa antiparasitario</option>
                                        <option value="6">Agregar Foto</option>
                                        <option value="7">PLAN DE VACUNACIONES</option>
                                    </select>
                                </div>
                        </div>
                 </div>
                <div class="col-md-12">
                    <div class="event evento-1">
                        
                               <input type="hidden" name="tipo" value="1">
                                <div class="row">
                                      <table id="pesos" class="table table-bordered  table-responsive ">
                                          <thead>
                                          <tr>
                                              <th>Caballo</th>
                                              <th>PESO</th>
                                              <th>FECHA INGRESO</th>
                                              <th>EDITAR</th>

                                          
                                          </tr>
                                          </thead>
                                          <tbody>
                                          @foreach($caballos as $c)
                                          <tr>
                                            <td>
                                            {{$c->nombre}}
                                            </td>
                                            <td>
                                            <input type="number"  class="input-pesos input-group" data-id="{{$c->id}}" name="caballo_pesos_{{$c->id}}" value={{$c->peso}}>
                                            </td>
                                            <td>
                                            {{$c->fecha_de_ingreso}}
                                            </td>
                                            <td>
                                            <a href="#" class="btn btn-info"> Editar </a>
                                            </td>
                                          </tr>
                                          @endforeach
                                          </tbody>
                                      </table>
                                </div>
                                

                        
                    </div>
                    <div class="event evento-2">
                        <form  method="post" action="{{url('eventos/create')}}" enctype="multipart/form-data">         {!! csrf_field() !!}
                            <input type="hidden" name="tipo" value="2">
                            <div class="row">
                                <div class="form-group col-xs-12 col-sm-8 col-md-5">
                                    <label for="caballo">Selecione Caballo</label> 
                                    <select  class="form-control required" multiple="multiple" id="caballos2" name="caballos[]">
                                    @foreach($caballos as $c)
                                        <option style="max-width:100px;" value="{{$c->id}}">{{$c->nombre}}</option>        
                                    @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                        <div class="col-xs-12 col-sm-8 col-md-5 ">
                                                    <label for="fecha">Fecha</label>
                                                    <input type="date" class="form-control required"  required name="fecha" id="fecha" placeholder="Fecha ">
                                    
                                    </div>
                            
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-sm-8 col-md-5 ">
                                                <label for="trabajo">Trabajo</label>
                                                <textarea class="form-control required"  required name="trabajo" id="trabajo"></textarea>
                            
                                    </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12 col-sm-8 col-md-5 ">
                                            <label for="observaciones">Observaciones</label>
                                            <textarea class="form-control required"   name="observaciones" id="observaciones" placeholder="observaciones"></textarea>
                            
                                </div>
                            </div>
                           <!-- <div class="row">
                                <div class="col-xs-12 col-sm-8 col-md-5 ">
                                <br>
                                        <label for="e2_file">Foto</label>
                                
                                        <input class="form-control " 
                                        type="file" id="e2_file" name="e2_file" />
                    
                                </div>
                            </div>  -->
                        <button type="submit" class="btn btn-info"> CREAR </button>
                        </form>
                     </div>   
                    <div class="event evento-3">
                        <form  method="post" action="{{url('eventos/create')}}" enctype="multipart/form-data">         {!! csrf_field() !!}
                              <input type="hidden" name="tipo" value="3">
                              <div class="row">
                                <div class="form-group col-xs-12 col-sm-8 col-md-5">
                                    <label for="caballo">Selecione Caballo</label> 
                                    <select  class="form-control required" multiple="multiple" id="caballos3" name="caballos[]">
                                    @foreach($caballos as $c)
                                        <option style="max-width:100px;" value="{{$c->id}}">{{$c->nombre}}</option>        
                                    @endforeach
                                    </select>
                                </div>
                            </div>
                              <div class="row">
                                    <div class="col-xs-12 col-sm-8 col-md-5 ">
                                                  <label for="fecha">Fecha</label>
                                                  <input type="date" class="form-control required"  required name="fecha" id="fecha" placeholder="Fecha ">
                                
                                     </div>
                        
                               </div>
                               <div class="row">
                                    <div class="col-xs-12 col-sm-8 col-md-5 ">
                                                  <label for="ovizq">OV Izquierdo</label>
                                                  <input type="text" class="form-control required"  required name="ovizq" id="ovizq" placeholder="Ov Izquierdo ">
                                
                                     </div>
                        
                               </div>
                               <div class="row">
                                    <div class="col-xs-12 col-sm-8 col-md-5 ">
                                                  <label for="ovder">OV Derecho</label>
                                                  <input type="text" class="form-control required"  required name="ovder" id="ovder" placeholder="Ov Derecho ">
                                
                                     </div>
                        
                               </div>
                               <div class="row">
                                    <div class="col-xs-12 col-sm-8 col-md-5 ">
                                                  <label for="utero">Utero</label>
                                                  <input type="text" class="form-control required"  required name="utero" id="utero" placeholder="Utero ">
                                
                                     </div>
                        
                               </div>
                               <div class="row">
                                    <div class="col-xs-12 col-sm-8 col-md-5 ">
                                                <label for="observaciones">Observaciones</label>
                                                <textarea class="form-control "  name="observaciones" id="observaciones" placeholder="observaciones"></textarea>
                              
                                    </div>
                                </div>
                                <div class="row">
                                  <div class="col-xs-12 col-sm-8 col-md-5 ">
                                  <br>
                                        <label for="e3_file">Foto</label>
                                
                                        <input class="form-control" type="file" id="e3_file" name="e3_file" accept="image/*" />
                    
                                  </div>
                              </div>  
                     

                        <button type="submit" class="btn btn-info"> CREAR </button>
                        </form>
                    </div>
                    <div class="event evento-4">
                        <form  method="post" action="{{url('eventos/create')}}" enctype="multipart/form-data">         {!! csrf_field() !!}
                              <input type="hidden" name="tipo" value="4">
                              <div class="row">
                                    <div class="form-group col-xs-12 col-sm-8 col-md-5">
                                        <label for="caballo">Selecione Caballo</label> 
                                        <select class="form-control" name="caballo" id="caballo4" >
                                        @foreach($caballos as $c)
                                                <option value="{{$c->id}}">{{$c->nombre}}</option>
                                        @endforeach
                                        </select>
                                    </div>
                                </div>
                              <div class="row">
                                    <div class="col-xs-12 col-sm-8 col-md-5 ">
                                                  <label for="fecha">Fecha</label>
                                                  <input type="date" class="form-control required"  required name="fecha" id="fecha" placeholder="Fecha ">
                                
                                     </div>
                        
                               </div>
                               
                                <div class="row">
                                    <div class="col-xs-12 col-sm-8 col-md-5 ">
                                                <label for="diagnostico">Diagnostico</label>
                                                <textarea class="form-control "   name="diagnostico" id="diagnostico" placeholder="Diagnostico"></textarea>
                              
                                    </div>
                                </div>
                                <div class="row">
                                  <div class="col-xs-12 col-sm-8 col-md-5 ">
                                  <br>
                                        <label for="file">Foto</label>
                                
                                        <input class="form-control "  type="file" id="file" name="file" />
                    
                                  </div>
                              </div>  
                                <div class="row">
                                      <div class="col-xs-12 col-sm-8 col-md-5">
                                          <br>
                                            <label for="e4_video">Video</label>
                                    
                                            <input class="form-control "  type="file" id="e4_video" name="e4_video" />
                        
                                      </div>
                                 </div>  
                                 <br>
                                 <div class="row">
                                        <div class="col-xs-12 col-sm-8 col-md-5 ">
                                                    <label for="tratamiento">Tratamiento</label>
                                                    <textarea class="form-control "   name="tratamiento" id="tratamiento" placeholder="tratamiento"></textarea>
                                  
                                        </div>
                                </div>
                                
                         <button type="submit" class="btn btn-info"> CREAR </button>
                        </form>

                    </div>

                    <div class="event evento-5">
                        <form  method="post" action="{{url('eventos/create')}}" enctype="multipart/form-data">         {!! csrf_field() !!}
                              <input type="hidden" name="tipo" value="5">
                              <div class="row">
                                <div class="form-group col-xs-12 col-sm-8 col-md-5">
                                    <label for="caballo">Selecione Caballo</label> 
                                    <select  class="form-control required" multiple="multiple" id="caballos5" name="caballos[]">
                                    @foreach($caballos as $c)
                                        <option style="max-width:100px;" value="{{$c->id}}">{{$c->nombre}}</option>        
                                    @endforeach
                                    </select>
                                </div>
                            </div>
                              <div class="row">
                                    <div class="col-xs-12 col-sm-8 col-md-5 ">
                                                  <label for="fecha">Fecha</label>
                                                  <input type="date" class="form-control required"  required name="fecha" id="fecha" placeholder="Fecha ">
                                
                                     </div>
                        
                               </div>
                            
                             
                               <div class="row">
                                    <div class="col-xs-12 col-sm-8 col-md-5 ">
                                    <label for="droga">Droga </label> 
                                    <select  class="form-control required" id="droga" name="droga">
                                            <option value="Equiplex 500. Mebendazol-Closantel"> Equiplex 500. Mebendazol-Closantel</option>    

                                            <option value="Equiplex IP. Ivermectina-Praziquantel"> Equiplex IP. Ivermectina-Praziquantel</option>     

                                            <option value="Doraquest.  Doramectina.">    Doraquest.  Doramectina.</option> 

                                            <option value=" Equest.  Moxidectina.">    Equest.  Moxidectina.</option> 

                                            <option value="Exodus.  Pamoato de Pirantel.">     Exodus.  Pamoato de Pirantel.</option> 

                                            <option value="Totalject.  Mebendazol- Triclorfon">Totalject.  Mebendazol- Triclorfon  </option>  
                                    </select>
                                     </div>
                        
                               </div>
                           <br>
                         <button type="submit" class="btn btn-info"> CREAR </button>
                        </form>

                    </div>
                    <div class="event evento-6">
                        <form  method="post" action="{{url('eventos/create')}}" enctype="multipart/form-data">         {!! csrf_field() !!}
                              <input type="hidden" name="tipo" value="6">
                              <div class="row">
                                    <div class="form-group col-xs-12 col-sm-8 col-md-5">
                                        <label for="caballo">Selecione Caballo</label> 
                                        <select class="form-control" name="caballo" id="caballo6" >
                                        @foreach($caballos as $c)
                                                <option value="{{$c->id}}">{{$c->nombre}}</option>
                                        @endforeach
                                        </select>
                                    </div>
                                </div>
                              <div class="row">
                                    <div class="col-xs-12 col-sm-8 col-md-5 ">
                                                  <label for="fecha">Fecha</label>
                                                  <input type="date" class="form-control required"  required name="fecha" id="fecha" placeholder="Fecha ">
                                
                                     </div>
                        
                               </div>
                                <div class="row">
                                  <div class="col-xs-12 col-sm-8 col-md-5 ">
                                  <br>
                                        <label for="e4_file">Foto</label>
                                
                                        <input class="form-control required" required type="file" id="e3_file" name="e4_file" />
                    
                                  </div>
                              </div>  
                     

                        <button type="submit" class="btn btn-info"> CREAR </button>
                        </form>
                    </div>
                    <div class="event evento-7">
                        <form  method="post" action="{{url('eventos/create')}}" enctype="multipart/form-data">         {!! csrf_field() !!}
                              <input type="hidden" name="tipo" value="7">
                              <div class="row">
                                <div class="form-group col-xs-12 col-sm-8 col-md-5">
                                    <label for="caballo">Selecione Caballo</label> 
                                    <select  class="form-control required" multiple="multiple" id="caballos7" name="caballos[]">
                                    @foreach($caballos as $c)
                                        <option style="max-width:100px;" value="{{$c->id}}">{{$c->nombre}}</option>        
                                    @endforeach
                                    </select>
                                </div>
                            </div>
                              <div class="row">
                                    <div class="col-xs-12 col-sm-8 col-md-5 ">
                                                  <label for="fecha">Fecha</label>
                                                  <input type="date" class="form-control required"  required name="fecha" id="fecha" placeholder="Fecha ">
                                
                                     </div>
                        
                               </div>
                            
                             
                               <div class="row">
                                    <div class="col-xs-12 col-sm-8 col-md-5 ">
                                    <label for="vacunas">Vacunas </label> 
                                    <select  class="form-control required" id="vacunas" name="vacunas">
                                            <option value="Vacuna Lexington 8 - Rino , Influenza, Tétanos y encefalomielitis"> Vacuna Lexington 8 - Rino , Influenza, Tétanos y encefalomielitis</option>    

                                            <option value="Vacuna Cultivac - Rinoneumonitis Influenza"> Vacuna Cultivac - Rinoneumonitis Influenza</option>     

                                            <option value="Vacuna Fluvac Innovator - Rinoneumonitis Influenza">    Vacuna Fluvac Innovator - Rinoneumonitis Influenza.</option> 

                                            <option value="Vacuna Fluvac Innovator H4 Aborto Viral">  Vacuna Fluvac Innovator H4 Aborto Viral</option> 

                                            <option value="Vacuna Tetanoc -Tetanos">    Vacuna Tetanoc -Tetanos</option> 

                                            <option value="Vacuna Adenosan - Adenitis equina">Vacuna Adenosan - Adenitis equina </option>  

                                            <option value="Vacuna Rhodovac - Rhodococus Equi"> Vacuna Rhodovac - Rhodococus Equi </option>  
                                           
                                    </select>
                                     </div>
                        
                               </div>
                           <br>
                         <button type="submit" class="btn btn-info"> CREAR </button>
                        </form>

                    </div>
                </div>
                
         <br>
        </div>
    </div>
</div>



@stop

@section("js")
<script src="{{asset('js/jquery.quicksearch.js')}}" type="text/javascript"></script>
<script src="{{asset('js/jquery.multi-select.js')}}" type="text/javascript"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js"></script>
  <script>


function matchCustom(params, data) {
    // If there are no search terms, return all of the data
    if ($.trim(params.term) === '') {
      return data;
    }

    // Do not display the item if there is no 'text' property
    if (typeof data.text === 'undefined') {
      return null;
    }

    // `params.term` should be the term that is used for searching
    // `data.text` is the text that is displayed for the data object
    if (data.text.indexOf(params.term) > -1) {
      var modifiedData = $.extend({}, data, true);
      modifiedData.text += ' (matched)';

      // You can return modified objects from here
      // This includes matching the `children` how you want in nested data sets
      return modifiedData;
    }

    // Return `null` if the term should not be displayed
    return null;
}
$('#caballo1,#caballo2,#caballo3,#caballo4,#caballo6').select2({
    matcher: matchCustom
});
    $( "#tipo_evento" ).change(function() {

      cambio_evento($(this).val());
    });
  
    $( document ).ready(function() {
      $(".evento-1").show();
      if(location.search.indexOf('event=')>=0){
    var event=location.search.substr(-1);
    if(event>0 && event <8){
      $('#tipo_evento option[value='+event+']').attr('selected','selected');
      cambio_evento(event);
    }else{
      cambio_evento(1);
    }
  }else{
    cambio_evento(1);
  }
     
});
function cambio_evento(event){
  $(".event").hide();
        
        $(".evento-"+event).show();

       
      if(event>1 && event!=6 && event!= 4 ){
            $('#caballos'+event).multiSelect({
                selectableHeader: "<input type='text' class=' form-control  search-input' autocomplete='off' placeholder='Buscar'>",
                  selectionHeader: "<input type='text' class=' form-control  search-input' autocomplete='off' placeholder='Buscar'>",
                  afterInit: function(ms){
                    var that = this,
                        $selectableSearch = that.$selectableUl.prev(),
                        $selectionSearch = that.$selectionUl.prev(),
                        selectableSearchString = '#'+that.$container.attr('id')+' .ms-elem-selectable:not(.ms-selected)',
                        selectionSearchString = '#'+that.$container.attr('id')+' .ms-elem-selection.ms-selected';

                    that.qs1 = $selectableSearch.quicksearch(selectableSearchString)
                    .on('keydown', function(e){
                      if (e.which === 40){
                        that.$selectableUl.focus();
                        return false;
                      }
                    });

                    that.qs2 = $selectionSearch.quicksearch(selectionSearchString)
                    .on('keydown', function(e){
                      if (e.which == 40){
                        that.$selectionUl.focus();
                        return false;
                      }
                    });
                  },
                  afterSelect: function(){
                    this.qs1.cache();
                    this.qs2.cache();
                  },
                  afterDeselect: function(){
                    this.qs1.cache();
                    this.qs2.cache();
                  }
                });
        }
        if(event==1){
          
          $('#pesos').DataTable( {
            "ordering": false,
                "pageLength": 100,
                "language":{
                    "sProcessing":     "Procesando...",
                    "sLengthMenu":     "Mostrar _MENU_ registros",
                    "sZeroRecords":    "No se encontraron resultados",
                    "sEmptyTable":     "Ningún dato disponible en esta tabla",
                    "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                    "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
                    "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                    "sInfoPostFix":    "",
                    "sSearch":         "Buscar:",
                    "sUrl":            "",
                    "sInfoThousands":  ",",
                    "sLoadingRecords": "Cargando...",
                    "oPaginate": {
                        "sFirst":    "Primero",
                        "sLast":     "Último",
                        "sNext":     "Siguiente",
                        "sPrevious": "Anterior"
                    },
                    "oAria": {
                        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                    }
                }

            } );
        }
       
}



$('.input-pesos').on('change', function () {

  
$.ajax({

    headers: {'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')},
    url: '/caballos/peso_update/'+$(this).data('id'), // point to server-side PHP script 
    data: {
      'peso': $(this).val()
                  },
    type: 'post',
    success: function (resp) {
        
        console.log("entry");
    },
    error: function (resp) {
    //	$('#msg').html(response); // display error response from the PHP script
    }
});



});
</script>

 
<style>
.event{
  display:none;
}
form button{
  margin-bottom: 40px!important;
    margin-top: 17px;
}
}
</style>
@stop
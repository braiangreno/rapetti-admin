<?php /**
 * Created by PhpStorm.
 * User: raffa
 * Date: 23/9/2018
 * Time: 2:35 PM
 */ ?>
@extends('adminlte::page')

@section('title', 'Evento: Revisión Veterinari - Haras - Rapetti')

@section('content_header')
    <h1>Evento: Revisión Veterinaria   <button class="btn btn-success" onclick=" window.history.back()">< Volver </button></h1>
@stop

@section('content')
<div class="row">
    <div class="box">
          <div class="row" style="margin-left:10px;">
            
                 <div class="col-md-12">
                    <div class="event evento-4">
           
                              <div class="row">
                                    <div class="col-xs-12 col-sm-8 col-md-5 ">
                                                  <label for="fecha">Fecha</label>
                                                  <input type="date" class="form-control required" value="{{$e->fecha }}" disabled  required name="fecha" id="fecha" placeholder="Fecha ">
                                
                                     </div>
                        
                               </div>
                                <div class="row">
                                    <div class="col-xs-12 col-sm-8 col-md-5 ">
                                                <label for="diagnostico">Diagnostico</label>
                                                <textarea class="form-control "  required   disabled name="diagnostico" id="diagnostico" >{{$e->diagnostico }}    </textarea>
                              
                                    </div>
                                </div>
                                <div class="row">
                                        <div class="col-xs-12 col-sm-8 col-md-5 ">
                                             <br>
                                              <label for="file">Foto</label>

                                              @if(!$fotos->isEmpty())
                                                @foreach($fotos as $f)
                                                        
                                                        <img class="profile-user-img img-responsive" style="width:auto;height:250px" src="{{$f->url}}" alt="picture">
                                        
                                                @endforeach
                                            @else
                                                <h3> NO HAY IMAGENES CARGADAS</h3>
                                            @endif
                                              
                                             
                                    </div>
                                </div>
                                <div class="row">
                                      <div class="col-xs-12 col-sm-8 col-md-5">
                                          <br>
                                            <label for="e4_video">Video</label>
                                            <br>
                                            <div class="text-center">
                                            
                                     @if($e->video_url=='')
                                       <h3>NO HAY VIDEO CARGADO</h3> 
                                    @else
                                            @if(strpos($e->video_url,'.mp4'))
                                            <video controls width="300">
                                                <source src="{{$e->video_url}}" type="video/mp4">
                                                Your browser does not support the audio element.
                                            </video>
                                            @else
                                                <h3>Video no valido</h3>
                                            @endif
                                    @endif
                                    </div>
                        
                                      </div>
                                 </div>  
                                 <br>
                                 <div class="row">
                                        <div class="col-xs-12 col-sm-8 col-md-5 ">
                                                    <label for="tratamiento">Tratamiento</label>
                                                    <textarea class="form-control "   name="tratamiento" id="tratamiento" disabled placeholder="">{{$e->tratamiento }}</textarea>
                                  
                                        </div>
                                </div>
                              

                    </div>
                </div>
         
        </div>
    </div>
</div>



@stop

@section("js")
  <script>

</script>
<style>
.event{
  margin-bottom: 40px!important;
    margin-top: 17px;

}
</style>
@stop
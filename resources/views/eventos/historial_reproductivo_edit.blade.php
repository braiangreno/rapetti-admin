<?php /**
 * Created by PhpStorm.
 * User: raffa
 * Date: 23/9/2018
 * Time: 2:35 PM
 */ ?>
@extends('adminlte::page')

@section('title', 'Evento: Historia Reproductiva - Haras - Rapetti')

@section('content_header')
    <h1>Evento: Historia Reproductiva   <button class="btn btn-success" style="float:right" onclick=" window.history.back()">< Volver </button></h1>
@stop

@section('content')
<div class="row">
    <div class="box">
          <div class="row" style="margin-left:10px;">
            
          <form  method="post" action="{{url('caballos/evento/editar/')}}/{{$e->id}}" enctype="multipart/form-data">    
               {!! csrf_field() !!}
                <div class="col-md-12">
                    <div class="event evento-3">
                              <div class="row">
                                    <div class="col-xs-12 col-sm-8 col-md-5 ">
                                                  <label for="fecha">Fecha</label>
                                                  <input type="date" class="form-control required"  required name="fecha" id="fecha"  value="{{$e->fecha }}" placeholder="Fecha ">
                                
                                     </div>
                        
                               </div>
                               <div class="row">
                                    <div class="col-xs-12 col-sm-8 col-md-5 ">
                                                  <label for="ovizq">OV Izquierdo</label>
                                                  <input type="text" class="form-control required"  required name="ovizq" id="ovizq"  value="{{$e->ovizq }}" placeholder="Ov Izquierdo ">
                                
                                     </div>
                        
                               </div>
                               <div class="row">
                                    <div class="col-xs-12 col-sm-8 col-md-5 ">
                                                  <label for="ovder">OV Derecho</label>
                                                  <input type="text" class="form-control required"  required name="ovder" id="ovder"  value="{{$e->ovder }}" placeholder="Ov Derecho ">
                                
                                     </div>
                        
                               </div>
                               <div class="row">
                                    <div class="col-xs-12 col-sm-8 col-md-5 ">
                                                  <label for="utero">Utero</label>
                                                  <input type="text" class="form-control required"  required name="utero" id="utero" value="{{$e->utero }}" placeholder="Utero ">
                                
                                     </div>
                        
                               </div>
                               <div class="row">
                                    <div class="col-xs-12 col-sm-8 col-md-5 ">
                                                  <label for="utero">Ecografia</label>
                                                  <input class="form-control" type="file" id="e3_file" name="e3_file" accept="image/*">
                                    @foreach ($errors->all() as $error)
                                         <span class="invalid-feedback " role="alert"> 
                                            <strong>El tamaño máximo de la imagen debe ser de 2048 KB</strong>
                                         </span> 
                                     @endforeach
                                     <img class="profile-user-img" style="width: 200px;height: 175px;top: 3px;position: relative;padding: 0px;margin-bottom: 10px;margin-top: 15px;" src="http://localhost/rapetti-admin/public/imagenes_subidas/{{$e->img_ecografia}}">
                                     </div> 

                                     

                        
                               </div>
                               <div class="row">
                                    <div class="col-xs-12 col-sm-8 col-md-5 ">
                                                <label for="observaciones">Observaciones</label>
                                                <textarea class="form-control required"  required name="observaciones" id="observaciones"  placeholder="observaciones">{{$e->observaciones }}</textarea>
                              
                                    </div>
                                </div>
                                <br>
                                <button type="submit" class="btn btn-info"> Actualizar </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>



@stop

@section("js")
  <script>

</script>
<style>

.event{
  margin-bottom: 40px!important;
    margin-top: 17px;

}
</style>
@stop
<?php /**
 * Created by PhpStorm.
 * User: raffa
 * Date: 23/9/2018
 * Time: 2:35 PM
 */ ?>
@extends('adminlte::page')

@section('title', 'Evento : Peso - Haras - Rapetti')

@section('content_header')
    <h1>Evento : Peso  <button class="btn btn-success" onclick=" window.history.back()">< Volver </button></h1>

@stop

@section('content')
<div class="row">
    <div class="box">
          <div class="row" style="margin-left:10px;">
 
                <div class="col-md-12">
                    <div class="event evento-1"> 
                                <div class="row">
                                            <div class="col-xs-12 col-sm-8 col-md-5 ">
                                                    <label for="peso">Peso</label>
                                                    <input type="number"  disabled step="0.01" min="1" max="10000" class="form-control required"  required name="peso" id="peso" value="{{$e->peso}}" placeholder="Peso">
                                                </div>
                                    </div>
                                <div class="row">
                                     <div class="col-xs-12 col-sm-8 col-md-5 ">
                                                <label for="fecha">Fecha</label>
                                                <input type="date" disabled class="form-control required"  required name="fecha" value="{{$e->fecha}}" id="fecha" placeholder="Fecha ">
                            
                                    </div>
                                </div>

                    </div>
                   
                    
                </div>
         
        </div>
    </div>
</div>



@stop

@section("js")
  <script>


</script>
<style>

.event{
  margin-bottom: 40px!important;
    margin-top: 17px;

}
</style>
@stop
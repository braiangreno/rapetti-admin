<?php /**
 * Created by PhpStorm.
 * User: raffa
 * Date: 23/9/2018
 * Time: 2:35 PM
 */ ?>
@extends('adminlte::page')

@section('title', 'Evento: Historia Reproductiva - Haras - Rapetti')

@section('content_header')
    <h1>Evento: Historia Reproductiva   <button class="btn btn-success" onclick=" window.history.back()">< Volver </button></h1>
@stop

@section('content')
<div class="row">
    <div class="box">
          <div class="row" style="margin-left:10px;">
            
              
                <div class="col-md-12">
                    <div class="event evento-3">
                              <div class="row">
                                    <div class="col-xs-12 col-sm-8 col-md-5 ">
                                                  <label for="fecha">Fecha</label>
                                                  <input type="date" class="form-control required"  required name="fecha" id="fecha" disabled value="{{$e->fecha }}" placeholder="Fecha ">
                                
                                     </div>
                        
                               </div>
                               <div class="row">
                                    <div class="col-xs-12 col-sm-8 col-md-5 ">
                                                  <label for="ovizq">OV Izquierdo</label>
                                                  <input type="text" class="form-control required"  required name="ovizq" id="ovizq" disabled value="{{$e->ovizq }}" placeholder="Ov Izquierdo ">
                                
                                     </div>
                        
                               </div>
                               <div class="row">
                                    <div class="col-xs-12 col-sm-8 col-md-5 ">
                                                  <label for="ovder">OV Derecho</label>
                                                  <input type="text" class="form-control required"  required name="ovder" id="ovder" disabled value="{{$e->ovder }}" placeholder="Ov Derecho ">
                                
                                     </div>
                        
                               </div>
                               <div class="row">
                                    <div class="col-xs-12 col-sm-8 col-md-5 ">
                                                  <label for="utero">Utero</label>
                                                  <input type="text" class="form-control required"  required name="utero" id="utero" value="{{$e->utero }}" placeholder="Utero ">
                                
                                     </div>
                        
                               </div>
                               <div class="row">
                                    <div class="col-xs-12 col-sm-8 col-md-5 ">
                                                <label for="observaciones">Observaciones</label>
                                                <textarea class="form-control required"  required name="observaciones" id="observaciones" disabled placeholder="observaciones">{{$e->observaciones }}</textarea>
                              
                                    </div>
                                </div>
                                <div class="row">
                                  <div class="col-xs-12 col-sm-8 col-md-5 ">
                                  <br>
                                        <label for="e3_file">Foto</label>
                                
                                    @if($e->foto_url=='')
                                        <img class="profile-user-img img-responsive" src="/imagenes_subidas/empty.png" alt="User profile picture">
                                    @else
                                        <img class="profile-user-img img-responsive" style="width:auto;height:250px" src="{{$e->foto_url}}" alt="picture">
                        
                                    @endif
                    
                                  </div>
                              </div>  
                             
                    </div>
                </div>
         
        </div>
    </div>
</div>



@stop

@section("js")
  <script>

</script>
<style>

.event{
  margin-bottom: 40px!important;
    margin-top: 17px;

}
</style>
@stop
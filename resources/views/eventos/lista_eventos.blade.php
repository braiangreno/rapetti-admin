<?php /**
 * Created by PhpStorm.
 * User: raffa
 * Date: 23/9/2018
 * Time: 2:35 PM
 */ ?>
@extends('adminlte::page')

@section('title', 'Eventos: Lista - Haras - Rapetti')

@section('content_header')
    <h1>Lista de eventos:<button class="btn btn-success"  style="float:right;" onclick=" window.history.back()">Volver </button></h1>
@stop

@section('content')
<div class="row">
<div class="box box-primary">
                <div class="box-header with-border">
                @switch($evento_type)



                            @case(2)

                       
                            <h3>  <span>HERRERO </span> </h3>
                            @break

                            @case(3)
                            <h3>  <span>HISTORIAS REPRODUCTIVAS</span> </h3>
                          
                            @break
                            @case(4)

                        <h3>  <span>SANIDADES</span> </h3>
                        @break
                            @case(5)

                             <h3>  <span>PROGRAMA ANTIPARASITARIO</span> </h3>
                            @break
                            @case(7)
                            <h3>  <span>VACUNAS</span> </h3>
                          
                            @break
           

                @endswitch

</div>

            
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                <table id="eventos" class="table table-bordered ">
              
                        @switch($evento_type)
                            @case(2)
                                            <thead>
                                                    <tr>
                                                <th>Fecha</th>
                                                <th>trabajo</th>
                                                <th>observaciones</th>
                            
                                            </tr>
                                            </thead>
                                            <tbody>
                                                    @foreach($eventos as $e)
                                                        <tr>
                                                            <td>
                                                            {{$e->fecha}}
                                                            </td>
                                                            <td>
                                                            {{$e->trabajo}}
                                                            </td>
                                                            <td>
                                                            {{$e->observaciones}}
                                                            </td>
                                                        
                                            
                                                        </tr>
                                                    @endforeach
                                            </tbody>
                              @break
                             @case(3)
                                       <thead>
                                            <tr>
                                                <th>Fecha</th>
                                                <th>ov izquierdo</th>
                                                <th>ov derecho</th>
                                                <th>Utero</th>
                                                <th>Observaciones</th>
                                                <th>Foto</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($eventos as $e)
                                                <tr>
                                                    <td>
                                                    {{$e->fecha}}
                                                    </td>
                                                    <td>
                                                    {{$e->ovizq}}
                                                    </td>
                                                    <td>
                                                    {{$e->ovder}}
                                                    </td>
                                                    <td>
                                                    {{$e->utero}}
                                                    </td>
                                                    <td>
                                                    {{$e->observaciones}}
                                                    </td>
                                                    <td>
                                                    @if($e->foto_url=="")
                                                                    Sin foto
                                                                @else
                                                                    <img class="img-responsive" style="width:auto;max-width:250px" src="{{$e->foto_url}}" alt="User profile picture">
                                                    
                                                                @endif
                                                            
                                                    </td>
                                            
                                                
                                    
                                                </tr>
                                            @endforeach
                                        </tbody>
                              @break

                       
                              @case(4)  
                                        <thead>
                                            <tr>
                                                 <th>Fecha</th>
                                                <th>Diagnostico</th>
            
                                                <th>Foto</th>
                                                <th>Video</th>
                                                <th>Tratamiento</th>

                                            </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($eventos as $e)
                                            <tr>
                                                <td>
                                                {{$e->fecha}}
                                                </td>
                                                <td>
                                                {{$e->diagnostico}}
                                                </td>
                                                <td>
                                                @if(!$fotos[$e->id]->isEmpty())
                                                    @foreach($fotos[$e->id] as $f)
                                                            
                                                            <img class="profile-user-img img-responsive" style="width:auto;height:150px" src="{{$f->url}}" alt="picture">
                                            
                                                    @endforeach
                                                @else
                                                    <h3> NO HAY IMAGENES CARGADAS</h3>
                                                @endif
                                                </td>
                                                <td>
                                                @if($e->video_url=='')
                                                   <h3>NO HAY VIDEO CARGADO</h3> 
                                                @else
                                                        @if(strpos($e->video_url,'.mp4'))
                                                        <video controls width="200">
                                                            <source src="{{$e->video_url}}" type="video/mp4">
                                                            Your browser does not support the audio element.
                                                        </video>
                                                        @else
                                                            <h3>Video no valido</h3>
                                                        @endif
                                                @endif
                                                </td>
                                                <td>
                                                {{$e->tratamiento}}
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                              @break
                            @case(5)  
                                        <thead>
                                            <tr>
                                                <th>Fecha</th>
            
                                                <th>Droga</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($eventos as $e)
                                            <tr>
                                                <td>
                                                {{$e->fecha}}
                                                </td>
                                                <td>
                                                {{$e->droga}}
                                                </td>
                                
                                            </tr>
                                        @endforeach
                                        </tbody>
                              @break

                            @case(7)   
                                        <thead>
                                          <tr>
                                                <th>Fecha</th>
                                                <th>Vacuna</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($eventos as $e)
                                            <tr>
                                                <td>
                                                {{$e->fecha}}
                                                </td>
                                                <td>
                                                {{$e->vacuna}}
                                                </td>                               
                                            </tr>
                                        @endforeach
                                        </tbody>
                              @break

                        @endswitch

                       
                </table>
            
            </div>
</div>



@stop

@section("js")
<script>
      $('#eventos').DataTable( {
        "order": [[0, "desc" ]],
                "language":{
                    "sProcessing":     "Procesando...",
                    "sLengthMenu":     "Mostrar _MENU_ registros",
                    "sZeroRecords":    "No se encontraron resultados",
                    "sEmptyTable":     "Ningún dato disponible en esta tabla",
                    "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                    "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
                    "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                    "sInfoPostFix":    "",
                    "sSearch":         "Buscar:",
                    "sUrl":            "",
                    "sInfoThousands":  ",",
                    "sLoadingRecords": "Cargando...",
                    "oPaginate": {
                        "sFirst":    "Primero",
                        "sLast":     "Último",
                        "sNext":     "Siguiente",
                        "sPrevious": "Anterior"
                    },
                    "oAria": {
                        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                    }
                }
            } );
</script>
<style>

.event{
  margin-bottom: 40px!important;
    margin-top: 17px;

}
</style>
@stop
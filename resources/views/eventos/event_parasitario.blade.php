<?php /**
 * Created by PhpStorm.
 * User: raffa
 * Date: 23/9/2018
 * Time: 2:35 PM
 */ ?>
@extends('adminlte::page')

@section('title', 'Evento: Parasitario - Haras - Rapetti')

@section('content_header')
    <h1>Evento: Revisión Parasitario   <button class="btn btn-success" onclick=" window.history.back()">< Volver </button></h1>
@stop

@section('content')
<div class="row">
    <div class="box">
          <div class="row" style="margin-left:10px;">
            
                 <div class="col-md-12">
                    <div class="event evento-4">

                              <div class="row">
                                    <div class="col-xs-12 col-sm-8 col-md-5 ">
                                                  <label for="fecha">Fecha</label>
                                                  <input type="date" class="form-control required" value="{{$e->fecha }}" disabled  required name="fecha" id="fecha" placeholder="Fecha ">
                                
                                     </div>
                        
                               </div>                    
            
                                <div class="row">
                                    <div class="col-xs-12 col-sm-8 col-md-5 ">
                                                <label for="Droga">Droga</label>
                                                <textarea class="form-control "   name="Droga" id="Droga" placeholder="Droga" disabled>{{$e->droga }}</textarea>
                              
                                    </div>
                                </div>

                    </div>
                </div>
         
        </div>
    </div>
</div>



@stop

@section("js")
  <script>

</script>
<style>
.event{
  margin-bottom: 40px!important;
    margin-top: 17px;

}
</style>
@stop
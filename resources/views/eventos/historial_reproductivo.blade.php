<?php /**
 * Created by PhpStorm.
 * User: raffa
 * Date: 23/9/2018
 * Time: 2:35 PM
 */ ?>
@extends('adminlte::page')

@section('title', 'Eventos: Historia Reproductiva - Haras - Rapetti')

@section('content_header')
    <h1>Evento: Historia Reproductiva   <button class="btn btn-success" onclick=" window.history.back()">< Volver </button></h1>
@stop

@section('content')
<div class="row">
<div class="box box-primary">
                <div class="box-header with-border">
                <h3 class="box-title">EVENTOS</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                <table id="eventos" class="table table-bordered ">
                        <thead>
                        <tr>
                            <th>Fecha</th>
                            <th>ov izquierdo</th>
                            <th>ov derecho</th>
                            <th>Utero</th>
                            <th>Observaciones</th>
                            <th>Foto</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($eventos as $e)
                            <tr>
                                <td>
                                {{$e->fecha}}
                                </td>
                                <td>
                                {{$e->ovizq}}
                                </td>
                                <td>
                                {{$e->ovder}}
                                </td>
                                <td>
                                {{$e->utero}}
                                </td>
                                <td>
                                {{$e->observaciones}}
                                </td>
                                <td>
                                @if($e->foto_url=="")
                                                Sin foto
                                            @else
                                                  <img class="img-responsive" style="width:auto;max-width:250px" src="{{$e->foto_url}}" alt="User profile picture">
                                  
                                            @endif
                                          
                                </td>
                           
                            </td>
                
                            </tr>
                        @endforeach

                        </tbody>
                    </table>
            
            </div>
</div>



@stop

@section("js")
<script>
      $('#eventos').DataTable( {
        "order": [[0, "desc" ]],
                "language":{
                    "sProcessing":     "Procesando...",
                    "sLengthMenu":     "Mostrar _MENU_ registros",
                    "sZeroRecords":    "No se encontraron resultados",
                    "sEmptyTable":     "Ningún dato disponible en esta tabla",
                    "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                    "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
                    "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                    "sInfoPostFix":    "",
                    "sSearch":         "Buscar:",
                    "sUrl":            "",
                    "sInfoThousands":  ",",
                    "sLoadingRecords": "Cargando...",
                    "oPaginate": {
                        "sFirst":    "Primero",
                        "sLast":     "Último",
                        "sNext":     "Siguiente",
                        "sPrevious": "Anterior"
                    },
                    "oAria": {
                        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                    }
                }
            } );
</script>
<style>

.event{
  margin-bottom: 40px!important;
    margin-top: 17px;

}
</style>
@stop
<?php /**
 * Created by PhpStorm.
 * User: raffa
 * Date: 23/9/2018
 * Time: 2:35 PM
 */ ?>
@extends('adminlte::page')

@section('title', 'Caballos - Haras - Rapetti')

@section('content_header')
    <h1>Caballos 
    </h1>
@stop

@section('content')
   
  <div class="row">
         <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="box">
                            <div class="box-body">
                                    <div class="row">
                                        @foreach($caballos as $c)
                                                <div class="ficha col-xs-12 col-sm-6 col-md-4 col-lg-3"> 
                                                        <a href="/caballos/perfil/prop/{{$c->id}}">
                                                                    <div class="col-xs-12" style="
                                                                    background-image:url({{$c->foto_perfil_url}});
                                                                    background-size:cover;
                                                                    background-position:center;
                                                                    height:35vh;

                                                                    border-radius:10px;
                                                                    "> 
                                                                                <h5 style="    position: absolute;
                                                                                        bottom: 4px;
                                                                                        color: white;
                                                                                        font-weight: 900;
                                                                                        font-size: 21px;
                                                                                    ">{{$c->nombre}}</h5>

                                                                                    <div class="ojo" >
                                                                                        <i class="fa fa-eye" style="font-size: 30px;"></i>
                                                                                    </div>
                                                                    </div> 
                                                            </a>
                                                </div> 
                                        @endforeach
                                
                                    </div>

                             </div>
                    </div>
          </div>




@stop

@section("js")
<style>
.ficha{
    margin-top:15px;
    margin-bottom:15px;
}
</style>
    <script>
   
    </script>

@stop
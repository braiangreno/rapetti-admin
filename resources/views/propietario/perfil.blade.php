<?php /**
 * Created by PhpStorm.
 * User: raffa
 * Date: 23/9/2018
 * Time: 2:35 PM
 */ ?>
@extends('adminlte::page')

@section('title', 'Caballos - Haras - Rapetti')

@section('content_header')
    <h1>Perfil Caballo  <button class="btn btn-success" onclick=" window.history.back()">< Volver </button></h1>
@stop

@section('content')
<?php 
use App\Models\VideoCaballo;
?>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.5.1/css/swiper.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.bundle.min.js"></script>
<div class="row">
    <div class="col-md-5" style="z-index: 9">
                  <div class="col-md-12">
                      <div class="col-xs-12">    
                              <!-- Profile Image -->
                               <div class="box box-primary">
                                    <div class="box-body box-profile">
                                                    @if($caballo->foto_perfil_id==0)
                                                        <img class="profile-user-img img-responsive img-circle" src="/imagenes_subidas/empty.png" alt="User profile picture">
                                                    @else
                                                        <img class="profile-user-img img-responsive img-circle" style="width:100px;height:100px" src="{{$caballo->foto_perfil_url}}" alt="User profile picture">
                                        
                                                    @endif
                                                        @if($caballo->pedigree!='')

                                                        <a class="btn btn-success" style="position: absolute;
                                                                            top: 10px;
                                                                            right: 10px;font-size: 12px;" target="_blank" href="{{$caballo->pedigree}}"> Pedigree</a>
                                                        @endif
                                            <form  id="foto-perfil" method="post" action="/caballos/cargarimagenperfil" enctype="multipart/form-data">
                                                {{ csrf_field() }}
                                                <input class="form-control" type="file" id="file" name="file" style="opacity:0;height:0;"/>
                                                <h3 class="profile-username text-center" >{{$caballo->nombre}}</h3>
                                                <input type="hidden" name="id" value="{{$id}}">
                                       
                                            </form>
                                            <ul class="list-group list-group-unbordered">
                                                        <li class="list-group-item">
                                                            <b>Fecha de nacimiento</b> <span class="pull-right">{{$caballo->fecha_de_nacimiento}}</span>
                                                        </li>
                                                        <li class="list-group-item">
                                                            <b>Fecha de ingreso</b> <span class="pull-right">{{$caballo->fecha_de_ingreso}}</span>
                                                        </li>
                                                        <li class="list-group-item">
                                                            <b>Sexo</b> <span class="pull-right">{{$caballo->sexo}}</span>
                                                        </li>
                                                        <li class="list-group-item">
                                                            <b>Peso</b> <span class="pull-right">{{$caballo->peso}}</span>
                                                        </li>
                                                        <li class="list-group-item">
                                                            <b>Pelo</b> <span class="pull-right">{{$caballo->pelo}}</span>
                                                        </li>
                                                        <li class="list-group-item">
                                                            <b>Padre</b> <span class="pull-right">{{$caballo->padre}}</span>
                                                        </li>
                                                        <li class="list-group-item">
                                                            <b>Madre</b> <span class="pull-right">{{$caballo->madre}}</span>
                                                        </li>
                                                        <li class="list-group-item">
                                                                <b>Alimentación</b><br>
                                                                @switch($caballo->alimentacion)
                                                                            @case(1)
                                                                                06.00 3kg avena<br>
                                                                                17:00 3kg avena  <br>                              
                                                                                <br>
                                                                                Pastoreo durante todo el día.<br>

                                                                                De noche 8kg alfalfa seca
                                                                            @break

                                                                            @case(2)

                                                                                Racion Puro Trato Potro Inicial (1,5% del peso vivo) en dos comidas diarias<br>
                                                                                Hemolitan 5cc por dia<br>
                                                                                Organew 5g por día<br>
                                                                                Aminomix 20g-40g por dia<br>
                                                                                <br>
                                                                                Pastoreo durante todo el día.<br>
                                                                                De noche 3-5 kg alfalfa.<br>
                                                                            @break

                                                                            @case(3)
                                                                                Racion Puro Trato Potro Final (1,5% del peso vivo) en dos comidas diarias<br>
                                                                                Organew 5g por día<br>
                                                                                Aminomix 50g por dia<br>
                                                                                Glicopan 40cc por dia<br>
                                                                                <br>
                                                                                25 kg de alfalfa (verde y seca mezclada) por día.<br>
                                                                            @break

                                                                            @case(4)
                                                                                06.00 3kg avena <br>
                                                                                17:00 3kg avena<br>
                                                                                <br>
                                                                                25 kg de alfalfa (verde y seca mezclada) por día.<br>
                                                                            @break
                                                                            @case(0)
                                                                                Alimentación no asignada.
                                                                            @break
                                                                    
                                                                        @endswitch              
                                                        </li>
                                                        <li class="list-group-item">
                                                                <strong><i class="fa fa-comments margin-r-5"></i> Observaciones</strong>

                                                                    <p class="text-muted">

                                                                    {{$caballo->observaciones}}
                                                                    </p>



                                                        </li>
                                                        @if(count(VideoCaballo::where('id_caballo',$id)->select("id_video as id")->get()->toArray())>0)
                                                            <li class="list-group-item">
                                                                <a href="/caballos/videos_prop/{{$caballo->id}}" style="width: 100%;font-size: 22px;" class="btn btn-info" title="galeria de videos">
                                                                    <i class="far fa-play-circle"></i> Videos
                                                                </a>
                                
                                                            </li>
                                                        @endif
                                            </ul>

                                          
                                        </div>
                                  <!-- /.box-body -->
                              </div>
                              <!-- /.box -->
                          
                           <!-- /.box -->
                      </div>
                 </div>
     </div>    
        <div class="col-md-6">
                                        <!-- Swiper -->
                                    <div class="swiper-container" id="slider1">
                                            <div class="swiper-wrapper">
                                                @foreach($fotos as $f)
                                                
                                                      <div class="swiper-slide" style="background-image:url({{$f->url}})">
                                                          <a href="{{$f->url}}"  download >
                                                            <img  style="height: 55px;margin: 10px;" src="/download.png">

                                                          </a>
                                                          @foreach($fotos_data as $fot)
                                                                @if($fot->foto_url==$f->url)
                                                                   <h4 style="position: absolute;color:#fff;margin-left:5px;bottom: 0;">{{$fot->fecha}}</h1>
                                                                @endif
                                                          @endforeach
                                                      </div>
                                                @endforeach
                                            </div>
                                        <!-- Add Pagination -->
                                        <div class="swiper-pagination"></div>
                                    </div>

                
                                </div>

        @if(count($pesos)>0)
        <div class="col-md-6">
                <!-- About Me Box -->
                  <div class="box box-primary">
                      <!-- /.box-header -->
                      <div class="box-body">

                            <div id="chartContainer" style="height: 370px; width: 100%;"></div>
                  </div>
                <!-- /.box-body -->
            </div>
            </div>
            <script>
                    window.onload = function () {
                        var chart = new CanvasJS.Chart("chartContainer", {
               
                        axisY:[{
                            title: "Peso",
                            lineColor: "#000",
                            tickColor: "#000",
                            labelFontColor: "BLUE",
                            interval: 30,
                            titleFontColor: "#C24642",
                           suffix: "kg"
                        }],
                        axisY2:[{
                            title: "",
                            lineColor: "#000",
                            tickColor: "#000",
                            labelFontColor: "BLUE",
                            titleFontColor: "#C24642",
                           suffix: "kg"
                        }],
                        
                        axisX:[{
                            title: "MESES",
                            lineColor: "#000",
                            interval: 1,
                            tickColor: "#000",
                            labelFontColor: "BLUE",
                            titleFontColor: "#C24642",
                         
                        }],

               
                        legend: {
                            cursor: "pointer",
                            itemclick: toggleDataSeries
                        },
                        data: [{
                            type: "line",
                            name: "MAX PESO",
                            color: "BLUE",
                            markerSize: 0,
                            markerType: "none",
                            showInLegend: true,
                            dataPoints: [
                                @foreach($peso_max as $key=>$value)
                                  <?php  echo   "{ x:".$key.", y:". $value ." },";?> 
                                @endforeach

                            ]
                        },
                        {
                            type: "line",
                            name: "MIN PESO",
                            color: "RED",
                            showInLegend: true,
                            markerSize: 0,
                            markerType: "none",
                            dataPoints: [
                                @foreach($peso_min as $key=>$value)
                                  <?php  echo   "{ x:".$key.", y:". $value ." },";?> 
                                @endforeach
                            ]
                        },
                        {
                            type: "line",
                            name: "PESO",
                            color: "GREEN",
                            showInLegend: true,
                            dataPoints: [
                                @foreach($pesos as $ps)
                                  <?php  if($ps->peso>0){
                                      echo   "{ x:".$ps->mes.", y:". $ps->peso ." },";
                                  }
                                
                                  ?> 
                                @endforeach
                            
                            ]
                        }]
                    });
                    chart.render();

                    function toggleDataSeries(e) {
                        if (typeof (e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
                            e.dataSeries.visible = false;
                        } else {
                            e.dataSeries.visible = true;
                        }
                        e.chart.render();
                    }

                }
            </script>
       @endif

        
      
        <div class="   @if(count($pesos)>0)col-md-11 @else col-md-6 @endif">
            <div class="box box-primary">
                <div class="box-header with-border">
                 <h3 class="box-title">EVENTOS</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                <div class="row">
                        @foreach([7,5,4,3,2] as $e)
                        <div class="col-xs-4 col-md-3">
                                @switch($e)

                                            @case(1)
                                                <a class="btn btn-info" href="/eventos/{{$caballo->id}}/1">
                                                <span>PESO</span>
                                                </a>
                                            
                                            @break

                                            @case(2)
                                            <a class="btn btn-info"  style="width: 100%;
                                                                            margin-top: 1rem;
                                                                            margin-bottom: 1rem;"  href="/caballos/eventos_lista/{{$caballo->id}}/2">
                                            <span>HERRERO</span>
                                            </a>
                                            @break

                                            @case(3)
                                                @if($evento_eco>0)
                                                    <a class="btn btn-info"  style="width: 100%;
                                                                                    margin-top: 1rem;
                                                                                    margin-bottom: 1rem;"  href="/caballos/eventos_lista/{{$caballo->id}}/3">
                                                    <span>HISTORIA REPRODUCTIVA</span> </a>
                                                @endif
                                            @break

                                            @case(4)
                                            <a class="btn btn-info"  style="width: 100%;
                                                                            margin-top: 1rem;
                                                                            margin-bottom: 1rem;"  href="/caballos/eventos_lista/{{$caballo->id}}/4">
                                            <span>SANIDAD </span> </a>
                                            @break
                                            @case(5)
                                            <a class="btn btn-info"  style="width: 100%;
                                                                            margin-top: 1rem;
                                                                            margin-bottom: 1rem;"  href="/caballos/eventos_lista/{{$caballo->id}}/5">
                                            <span>PROGRAMA ANTIPARASITARIO</span> </a>
                                            @break
                                            @case(6)
                                            <a class="btn btn-info" href="/eventos/{{$caballo->id}}/6">
                                            <span>FOTO</span> </a>
                                            @break
                                            @case(7)
                                            <a class="btn btn-info"  style="width: 100%;
                                                                            margin-top: 1rem;
                                                                            margin-bottom: 1rem;"  href="/caballos/eventos_lista/{{$caballo->id}}/7">
                                            <span>VACUNAS</span> </a>
                                            @break
                                        @default

                                @endswitch
                        </div>
                        @endforeach
                    </div>
                </div>
             </div>
        
         </div>
         
        </div>
</div>


@stop

@section("js")

<script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.5.1/js/swiper.js"></script>
  <script>
    document.getElementById('upload').addEventListener('click', openDialog);

    function openDialog() {
      document.getElementById('file').click();
    }
    $('#foto-perfil input').change(function() {
         $(this).closest('form').submit();
    });

    $('#eventos').DataTable( {
        "order": [[0, "desc" ]],
                "language":{
                    "sProcessing":     "Procesando...",
                    "sLengthMenu":     "Mostrar _MENU_ registros",
                    "sZeroRecords":    "No se encontraron resultados",
                    "sEmptyTable":     "Ningún dato disponible en esta tabla",
                    "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                    "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
                    "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                    "sInfoPostFix":    "",
                    "sSearch":         "Buscar:",
                    "sUrl":            "",
                    "sInfoThousands":  ",",
                    "sLoadingRecords": "Cargando...",
                    "oPaginate": {
                        "sFirst":    "Primero",
                        "sLast":     "Último",
                        "sNext":     "Siguiente",
                        "sPrevious": "Anterior"
                    },
                    "oAria": {
                        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                    }
                }
            } );
</script>
  <style>
    
    .swiper-container {
      width: 100%;

      padding-bottom: 50px;
    }
    .swiper-slide {
      background-position: center;
      background-size: cover;
      width:60%;
      height: 300px;
    }
  </style>
  <!-- Initialize Swiper -->
  <script>
    var swiper = new Swiper('#slider1', {
      effect: 'coverflow',
      grabCursor: true,
      centeredSlides: true,
      slidesPerView: 'auto',
      coverflowEffect: {
        rotate: 50,
        stretch: 0,
        depth: 100,
        modifier: 1,
        slideShadows : true,
      },
      pagination: {
        el: '.swiper-pagination',
      },
    });
  </script>
@stop